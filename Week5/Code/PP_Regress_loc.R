# Author: Michael Schmutzer (michael.schmutzer12@ic.ac.uk)

# Fits several regression models for the relation between predator & 
# prey mass, for each feeding type and lifestage of the predator.
# Plots these models, and returns a summary of them in a new .csv file.

library(ggplot2)
library(scales)
library(grid)
library(plyr)

# Read in data
Predpry <- read.csv("../Data/EcolArchives-E089-51-D1.csv")

################ Plotting figure #######################

MassPlot <- ggplot(Predpry, 
                   aes(x = Prey.mass, 
                       y = Predator.mass,
                       colour = Predator.lifestage
                       )) +
                             
  geom_point(shape = 3) +
  scale_x_log10() +
  scale_y_log10() +
  facet_wrap(~ Location) +
  geom_smooth(method = lm, fullrange = TRUE) +
  xlab ("Prey Mass in grams") +
  ylab ("Predator mass in grams") +
  theme_bw() +
  theme(#plot.margin = unit(c(0.5,3.5,0.5,3.5), "cm"),
        panel.grid.major.x = element_line(),
        panel.grid.major.y = element_line(),
        panel.grid.minor.x = element_line(),
        panel.grid.minor.y = element_line(),
        legend.position = c("bottom"),
        strip.text.x = element_text(size = 3)
        )

pdf("../Results/PP_Regress_loc_Figure.pdf")
print(MassPlot)
dev.off()

################### Return regression results ##################

# Split dataframe and apply linear regression as above

# Define function that creates a data frame containing 
# regression results. ddply (next step) works best when 
# the function returns a dataframe

linearMod <- function(df){
  Model <- lm(log(Predator.mass) ~ log(Prey.mass), data = df)
  Model.summary <- summary(Model)
  mr <- data.frame(Slope = Model.summary$coefficients[2])
  mr$Intercept <- Model.summary$coefficients[1]
  mr$R.squared <- Model.summary$r.squared
  mr$F.value <- Model.summary$fstatistic[1]
  mr$P.value <- anova(Model)$'Pr(>F)'[1]
  return(mr)
}

# Apply function accross subsetted Predpry data 

Regres <- ddply(Predpry,
                c(
                  "Location", 
                  "Predator.lifestage"
                  ),
                function(df) linearMod(df)
                )

# Save regression results in new .csv file

write.csv(Regres, "../Results/PP_Regress_loc_Results.csv")


