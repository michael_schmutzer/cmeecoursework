# Author = Michael Schmutzer (michael.schmutzer12@ic.ac.uk)
# Demonstration of how using loops in R slows it down

M <- matrix(runif(1000000), 1000, 1000)
SumAllElements <- function(M){
  Dimensions <- dim(M)
  Tot <- 0
  for (i in 1:Dimensions[1]){
    for (j in 1:Dimensions[2]){
      Tot <- Tot + M[i,j]
    }
  }
  return(Tot)
}

# Test for time elapsed, sums is 100x faster!
print(system.time(SumAllElements(M)))

print(system.time(sum(M)))
