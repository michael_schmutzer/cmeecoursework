## Demonstration of Control statements

# An if / or statement
a <- TRUE
if (a == TRUE) 
{
  print ( "a is TRUE")
} else 
  {
    print("a is FALSE")
  }

# Another example
z <- runif(1)
if (z <= 0.5)
{
  print("Less than a quarter")
}

# Sequential For loop
for ( i in 1:100)
{
  j <- i * i
  print(paste(i, " squared is", j))
}

# For loop over a vector

v1 <- c("a", "bc", "def")
for (i in v1)
{
  print(i)
}

# As long as... / while

i <- 0
while (i < 100)
{
   i <- i + 1
   print(i ^ 2)
}
