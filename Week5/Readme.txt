Week6 

in ./Code/
-------------------
apply.R					# Example of the apply function run on a matrix
basic_io.R				# Example of input/output in R
break.R					# Example of breaking out of a loop using break
browse.R				# Example of debugging using browser()
control.R				# Demonstration of control statements
get_TreeHeight.R		# Calculates tree height through trigonometry. Reads filename from command line. 
GPDDmap.R				# Creates & saves map of locations in GPD database
next.R					# Example of next: skip certain iterations in a for loop
PP_Lattice.R			# Creates & saves lattice plots, returns summary table in ./Results		
PP_Regress_loc.R		# Same as PP_regress, but according to location & feeding interaction
PP_Regress.R			# Fits linear models according to lifestage & feeding interaction. Plots models and returns model summaries in a new file
Ricker.R				# Plots graph of Ricker's population model
run_get_TreeHeight.sh	# runs get_TreeHeight from bash
sample.R				# Example of vectorization using lapply()
TAutoCorr.R				# Calculates correlation between successive years, compares to null distribution
TreeHeight.R			# Estimates tree height using trigonometry
try.R					# Demonstration of try
Vectorize1.R			# Sums matrix along rows and columns using a loop
Vectorize2.R			# Applies Ricker equation on a matrix using for loops, and vectorized. Compare processing time
