This weeks folder contains simple scripts demonstrating the use of 
python, and how to annotate and test python code. Input used for these 
examples are stored in ./Data/, outputs from python scripts in ./Results/. Some 
input/output is stored in ./Sandbox/


Python scripts in ./Code/ directory
------------------------------------

align_seqs_fasta.py					# Read .fasta files, align as in align_seqs.py
align_seqs.py						# Produces a .csv file containing two sequences. Reads file and aligns sequences. Returns text file with best alignment and its score.
basic_csv.py						# Reads .csv file, prints each row, and prints species name. Extracts species name and body mass, and saves them in a seperate file.
basic_io.py							# Opens and prints files in Sandbox. Saves a dictionary, and reopens it.
blackbirds.py						# Returns kingdom, phylum, and species name for bird species in ../Data/blackbirds.txt using a regular expression
boilerplate.py						# Prints 'This is a boilerplate'
cfexercises.py						# Demonstration of for loops, prints 'hello' and performs several mathematical operations
control_flow.py						# Contains functions answering some mathematical questions 
dictionary.py						# Matches a set of species names to their order name in a dictionary from a list of tuples.
lc1.py								# Reallocates content from a tuple into 3 lists, using either list comprehension or loops.
lc2.py								# Prints months with average UK rainfall exceeding >100 or <50 mm.
loops.py							# Some simple loops printing strings, dictionaries, and numbers.
oaks.py								# Prints oak species names from a list that includes other trees.
re4.py								# Searches for email addresses using regular expressions.
regex_date.py						# Regular expression that can recognize a date in the format YYYYMMDD.
regexs.py							# Searches strins using regular expressions.
scope.py							# Demonstration of global and local variables. Prints both kind of variables, shows how variables can be shared by all the functions in a module.
sysargv.py							# Shows how sys.argv stores argument variables.
test_control_flow.py				# Some functions that demonstrate the use of control flows. Docstring tests.
tuple.py							# Prints tuples within a tuple, each on a seperate line.
using_name.py						# Demonstrates how python can track whether a module is run by itself or from another

