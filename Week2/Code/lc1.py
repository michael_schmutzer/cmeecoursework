#!/user/bin/env python

"""Reallocates content from a tuple into 3 lists, using either list 
comprehension or loops."""

__author__ = 'Michael Schmutzer (michael.schmutzer12@ic.ac.uk)'
__version__ = '0.0.1'

birds = ( ('Passerculus sandwichensis','Savannah sparrow',18.7),
          ('Delichon urbica','House martin',19),
          ('Junco phaeonotus','Yellow-eyed junco',19.5),
          ('Junco hyemalis','Dark-eyed junco',19.6),
          ('Tachycineata bicolor','Tree swallow',20.2),
         )

#(1) Write three separate list comprehensions that create three different
# lists containing the latin names, common names and mean body masses for
# each species in birds, respectively. 

# List of latin names
species_latin_comp = list(row[0] for row in birds)
print species_latin_comp

# List of common names
species_common_comp = list(row[1] for row in birds)
print species_common_comp

# List of body masses
body_mass_comp = list(row[2] for row in birds)
print body_mass_comp

# (2) Now do the same using conventional loops (you can choose to do this 
# before 1 !). 

# List containing latin names
species_latin_loop = []
for row in range(len(birds)):
	species_latin_loop.append(birds[row][0])
print species_latin_loop
 
# List containing common names
species_common_loop = []
for row in range(len(birds)):
	species_common_loop.append(birds[row][1])
print species_common_loop

# List containing body masses
body_mass_loop = []
for row in range(len(birds)):
	body_mass_loop.append(birds[row][2])
print body_mass_loop
