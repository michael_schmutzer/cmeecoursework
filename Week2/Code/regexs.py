#!/user/bin/env python

"""Searching strings using regular expression"""


__author__ = 'Michael Schmutzer (michael.schmutzer12@ic.ac.uk)'
__version__ = '0.0.1'

import re

my_string = "a given string"
#find a space within the string
match = re.search(r'\s', my_string)

print match

match.group() # this should return ' '

match = re.search(r's\w*', my_string)

match.group() # this should return 'string'

# If there is no match
match = re.search(r'\d', my_string)

print match

# Another example

str = 'an example'
match = re.search(r'\w*\s', str)
if match:
	print 'found a match:', match.group()
else:
	print 'did not find a match'

# Basic examples

match = re.search(r'\d' , "it takes 2 to tango")
print match.group() # prints 2

match = re.search(r'\s\w*\s', "once upon a time")
print match.group() # ' upon '

match = re.search(r'\s\w{1,3}\s', "once upon a time")
print match.group() # ' a '

match = re.search(r'\s\w*$', "once upon a time")
print match.group() # ' time ' 

match = re.search(r'\w*\s\d.*\d', "take 2 grams of H2O")
print match.group() # 'take 2 grams of H2'

match = re.search(r'^\w*.*\s', "once upon a time")
print match.group() # 'once upon a '

# Less greedy verion - use ?
match = re.search(r'^\w*.*?\s', "once upon a time")
print match.group() # 'once '

match = re.search(r'<.+>', "This is a <EM>first</EM> test")
print match.group() # '<EM>first</EM>'

# Make + lazy!
match = re.search(r'<.+?>', "This is a <EM>first</EM> test")
print match.group() # '<EM>'

match = re.search(r'\d*\.?\d*', "1432.75+60.221i")
print match.group() '1432.75'

match = re.search(r'\d*\.?\d*', "1432+60.22i")
print match.group() '1432'

match = re.search(r' [AGTC]+', "the sequence ATTCGT")
match.group() #'ATTCGT'

re.search(r'\s+[A-Z]{1}\w+\s\w+', "The bird-shit frog''s name is Theloderma asper").group()


