#!/user/bin/env python

"""Prints 'This is a boilerplate'."""

__author__ = 'Michael Schmutzer (michael.schmutzer12@ic.ac.uk)'
__version__ = '0.0.1'

# Imports

import sys # module to interface program with the operating system

# Constants can go here

# Functions can go here
def main(argv):
	"""This function does the printing"""
	print 'This is a boilerplate' # NOTE single indent
	# print 'control_flow says: ' + control_flow.even._or_odd(10) + '!' # NOTE indented using 2 tabs
	return 0

if (__name__ == "__main__"): #makes sure the "main" function is called from commandline
	status = main(sys.argv)
	# sys.exit(status)
