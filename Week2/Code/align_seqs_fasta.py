#!/user/bin/env python

"""Reads sequences from two .fasta files, and aligns them. Returns best 
alignment in a .txt file."""

__author__ = 'Michael Schmutzer (michael.schmutzer12@ic.ac.uk)'
__version__ = '0.0.1'

import re

def open_fasta(path):
	"""Opens .fasta file"""
	fasta1 = open(path, 'r')
	seq = re.sub(r'>gi.*\n', '', str(fasta1.read())).replace('\n', '')
	return seq

seq1 = open_fasta('../Data/407228326.fasta')
seq2 = open_fasta('../Data/407228412.fasta')


# assign the longest sequence s1, and the shortest to s2
# l1 is the length of the longest, l2 that of the shortest

l1 = len(seq1)
l2 = len(seq2)
if l1 >= l2:
	s1 = seq1
	s2 = seq2
else:
	s1 = seq2
	s2 = seq1
	l1, l2 = l2, l1 # swap the two lengths

# function that computes a score
# by returning the number of matches 
# starting from arbitrary startpoint
def calculate_score(s1, s2, l1, l2, startpoint):
	
	"""Determines how well the two sequences are matched. 
	Produces string showing matching positions. 
	Calculates alignment score."""
	
	# startpoint is the point at which we want to start
	matched = "" # contains string for alignement
	score = 0
	for i in range(l2):
		if (i + startpoint) < l1:
			# if its matching the character
			if s1[i + startpoint] == s2[i]:
				matched = matched + "*"
				score = score + 1
			else:
				matched = matched + "-"

	# build some formatted output
	print "." * startpoint + matched           
	print "." * startpoint + s2
	print s1
	print score 
	print ""

	return score

calculate_score(s1, s2, l1, l2, 0)
calculate_score(s1, s2, l1, l2, 1)
calculate_score(s1, s2, l1, l2, 5)

# now try to find the best match (highest score)
my_best_align = None
my_best_score = -1

for i in range(l1): 
	z = calculate_score(s1, s2, l1, l2, i)
	if z > my_best_score:
		my_best_align = "." * i + s2
		my_best_score = z

# Create a list containing summary output
align_res = [ 
			"Best alignment:",
			' ',
			my_best_align,
			s1,
			' ',
			"Best score:", 
			my_best_score
			]

# Save output to new file alignment.txt
out = open('../Results/fastaalignment.txt', 'w')

for i in align_res:
	out.write(str(i) + '\n')
	
out.close()
