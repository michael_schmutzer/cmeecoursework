#!/user/bin/env python
""" Some functions that demonstrate the use of control flows through several
mathematical operations."""

# Docstrings are part of the running code - other comments are ignored
# thus, they can be accessed during run time

__author__ = 'Michael Schmutzer (michael.schmutzer12@ic.ac.uk)'
__version__ = '0.0.1'

# Imports

import sys # module to interface program with the operating system

def even_or_odd(x = 0): #if not specified, x should be 0
	
	"""Find whether a number x is even or odd."""
	if x % 2 == 0: # Conditional if, the % gives the remainder upon division
		return "%d is Even!" % x
	return "%d is Odd!" % x

def largest_divisor_five(x = 120):
	"""Find which is the largest divisor of x among 2, 3, 4, 5."""
	largest = 0
	if x % 5 == 0: #means "else, if"
		largest = 5
	elif x % 4 == 0:
		largest = 4
	elif x % 3 == 0:
		largest = 3
	elif x % 2 == 0:
		largest = 2
	else: #When all other (if, elif) conditions are not met
		return "No divisor found for %d!" % x #Each function can return a value or a variable
		
def is_prime (x = 70):
	"""Find whether an integer is prime."""
	for i in range(2, x): # range produces as sequence of integers.
		if x % i == 0:
			print "%d is not a prime: %d is a divisor" % (x, i) 
		# print formatted text "%d %s %f %e" % (20, "30",0.0002,0.00003)			
		
			return False
	print "%d is a prime!" % x
	return True

def find_all_primes(x = 22):
	"""Find all the primes up to x."""
	allprimes = []
	for i in range(2, x + 1):
		if is_prime(i):
			allprimes.append(i)
	print "There are %d primes between 2 and %d" % (len(allprimes), x)
	return allprimes
	
def main(argv):
	#sys.exit #(don't want to do that right now!")
	print even_or_odd(22)
	print even_or_odd(33)
	print largest_divisor_five(120)
	print largest_divisor_five(121)
	print is_prime(60)
	print is_prime(59)
	print find_all_primes(100)
	return 0
	
if (__name__ == "__main__"):
	status = main(sys.argv)
	sys.exit(status)
