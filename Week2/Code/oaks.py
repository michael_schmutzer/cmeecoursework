#!/user/bin/env python

"""Print oak species names from a list that includes other trees."""

__author__ = 'Michael Schmutzer (michael.schmutzer12@ic.ac.uk)'
__version__ = '0.0.1'

taxa = [ 'Quercus robur',
		 'Fraxinus excelsior',
		 'Pinus sylvestris',
		 'Quercus cerris',
		 'Quercus petraea',
		]

# Return only oak trees from species list

def is_an_oak(name):
	return name.lower().startswith( 'quercus ')

# Create new set for just oaks using a for loop
oaks_loops = set()
for species in taxa:
	if is_an_oak(species):
		oaks_loops.add(species)
print oaks_loops

# List comprehension
oaks_lc = set([species for species in taxa if is_an_oak(species)])
print oaks_lc

# Names in uppercase using loops
oaks_loops = set()
for species in taxa:
	if is_an_oak(species):
		oaks_loops.add(species.upper())
print oaks_loops

# Upper case using list comprehensions
oaks_lc = set([species.upper() for species in taxa if is_an_oak(species)])
print oaks_lc
