#!/user/bin/env python

"""Returns kingdom, phylum, and species name for bird species in ../Data/blackbirds.txt
using a regular expression."""

__author__ = 'Michael Schmutzer (michael.schmutzer12@ic.ac.uk)'
__version__ = '0.0.1'

import re

# Read the file
f = open('../Data/blackbirds.txt', 'r')
text = f.read()
f.close()

# remove \t\n and put a space in:
text = text.replace('\t',' ')
text = text.replace('\n',' ')

# note that there are "strange characters" (these are accents and
# non-ascii symbols) because we don't care for them, first transform
# to ASCII:
text = text.decode('ascii', 'ignore')

# Now write a regular expression my_reg that captures # the Kingdom, 
# Phylum and Species name for each species and prints it out neatly:

my_reg = re.findall(r'(Kingdom\s\w+\s)[\w\s,]*(Phylum\s\w+\s)[\w\s,-]*(Species\s\w+\s\w+\s)', text)
for line in my_reg:
	print line


re.findall(r'Kingdom\s\w+\s', text)
# Hint: you will probably want to use re.findall(my_reg, text)...
# Keep in mind that there are multiple ways to skin this cat! 
