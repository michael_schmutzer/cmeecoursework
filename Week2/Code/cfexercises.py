#!/user/bin/env python

"""Some modules and functions demonstrating the use of (conditional) loops
 for repetitve tasks, including some maths"""

__author__ = 'Michael Schmutzer (michael.schmutzer12@ic.ac.uk)'
__version__ = '0.0.1'

import doctest # to use docstrings for testing

for i in range(3,17): # prints 14 times
	"""Prints 'hello' 14 times"""
	print 'hello'
	
for j in range(12): # prints 4 times
	"""Prints 'hello' 4 times"""
	if j % 3 == 0: # when j / 3 gives a remainder of 0
		print 'hello'

z = 0
while z != 15: # if z is not equal to 5...
	"""Prints 'hello' 5 times"""
	print 'hello'
	z = z + 3
	
z = 12
while z < 100: # prints 8 times
	""" Prints 'hello' 8 times"""
	if z == 31:
		for k in range(7):
			print 'hello'
	elif z == 18:
		print 'hello'
	z = z + 1
	
def foo1(x): # raises to the power of 0.5
	"""Raises numerical input to the power of 0.5
	
	>>> foo1(2)
	1.4142135623730951
	
	"""
	return x ** 0.5

def foo2(x, y): # returns the larger of 2 numbers
	"""Returns the larger of two numbers 
	
	>>> foo2(3,4)
	4
	
	"""
	if x > y:
		return x
	return y

def foo3(x, y, z): # Switches the order of numbers if the preceding is larger
	"""Switches two numbers once if the preceeding is larger
	
	>>> foo3(4,3,2) 
	[3, 2, 4]
	
	"""
	if x > y:
		tmp = y 
		y = x
		x = tmp
	if y > z:
		tmp = z
		z = y
		y = tmp
	return [x, y, z]
	
def foo4(x): # finds the factorial of its input
	"""Finds the factorial of a number
	
	>>> foo4(5)
	120
	
	"""
	result = 1
	for i in range(1, x + 1):
		result = result * i
	return result
	
def foo5(x): # Recusive loop... calls itself!
	"""Finds the factorial of a number
	
	>>> foo5(5)
	120
	
	"""
	if x == 1:
		return 1
	return x * foo5 (x - 1)

doctest.testmod()
