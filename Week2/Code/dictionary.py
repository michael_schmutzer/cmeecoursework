#!/user/bin/env python

__Author__ = 'Michael Schmutzer (michael.schmutzer12@ic.ac.uk)'
__Version__ = '0.0.1'

"""Matches a set of species names to their order name in a dictionary 
from a list of tuples."""

taxa = [ ('Myotis lucifugus','Chiroptera'),
         ('Gerbillus henleyi','Rodentia',),
         ('Peromyscus crinitus', 'Rodentia'),
         ('Mus domesticus', 'Rodentia'),
         ('Cleithrionomys rutilus', 'Rodentia'),
         ('Microgale dobsoni', 'Afrosoricida'),
         ('Microgale talazaci', 'Afrosoricida'),
         ('Lyacon pictus', 'Carnivora'),
         ('Arctocephalus gazella', 'Carnivora'),
         ('Canis lupus', 'Carnivora'),
        ]

# Write a short python script to populate a dictionary called taxa_dic 
# derived from  taxa so that it maps order names to sets of taxa. 
# E.g. 'Chiroptera' : set(['Myotis lucifugus']) etc. 

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS

# Write your script here:

# NB: in ipython sets are represented by {} as well!

# Prepare set of order names to search for in the next step
order_names = set(row[1] for row in taxa)

# Allocate the right species to each order in taxa_dic
taxa_dic = {}
for order in order_names:
	taxa_dic[order] = set(row[0] for row in taxa if row[1] is order)
print taxa_dic
