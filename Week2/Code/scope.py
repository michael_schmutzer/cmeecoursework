#!/user/bin/env python

"""Demonstration of global and local variables. Prints both kind of variables,
shows how variables can be shared by all the functions in a module."""

__author__ = 'Michael Schmutzer (michael.schmutzer12@ic.ac.uk)'
__version__ = '0.0.1'

_a_global = 10
def a_function():
	"""Prints local and global variables. Does not modify global variable."""
	_a_global = 5
	_a_local = 4
	print "Inside the function, the value is ", _a_global
	print "Inside the function, the value is ", _a_local
	return None
	
a_function()
print "Outside the function, the value is ", _a_global

# Slightly different... now adding 'global'

_a_global = 10
def a_function():
	"""Prints global and local variable. Modifies global variable."""
	global _a_global 
	_a_global = 5
	_a_local = 4
	print "Inside the function, the value is ", _a_global
	print "Inside the function, the value is ", _a_local
	return None
	
a_function()
print "Outside the function, the value is ", _a_global
