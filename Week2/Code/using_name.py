#!/user/bin/env python

"""Demonstrates how python can track whether a module is run by itself
or from another."""

__author__ = 'Michael Schmutzer (michael.schmutzer12@ic.ac.uk)'
__version__ = '0.0.1'

if __name__ == '__main__':
	print 'This program is being run by itself'
else:
	print 'I am being imported from another module'
