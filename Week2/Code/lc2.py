#!/user/bin/env python

"""Prints months with average UK rainfall exceeding >100 or <50 mm."""

__author__ = 'Michael Schmutzer (michael.schmutzer12@ic.ac.uk)'
__version__ = '0.0.1'

# Average UK Rainfall (mm) for 1910 by month
# http://www.metoffice.gov.uk/climate/uk/datasets
rainfall = (('JAN',111.4),
            ('FEB',126.1),
            ('MAR', 49.9),
            ('APR', 95.3),
            ('MAY', 71.8),
            ('JUN', 70.2),
            ('JUL', 97.1),
            ('AUG',140.2),
            ('SEP', 27.0),
            ('OCT', 89.4),
            ('NOV',128.4),
            ('DEC',142.2),
           )

# (1) Use a list comprehension to create a list of month,rainfall tuples where
# the amount of rain was greater than 100 mm.
 
rain_greater_comp = [row for row in rainfall if row[1] > 100] 
print rain_greater_comp
 
# (2) Use a list comprehension to create a list of just month names where the
# amount of rain was less than 50 mm. 

rain_less_comp = [row[0] for row in rainfall if row[1] < 50]
print rain_less_comp

# (3) Now do (1) and (2) using conventional loops (you can choose to do 
# this before 1 and 2 !).

# Months in which rainfall > 100 mm, including rainfall
rain_greater_loop = []
for row in rainfall:
	if row[1] > 100:
		rain_greater_loop.append(row)
print rain_greater_loop
	
# Months with rainfall < 50 mm, excluding rainfall
rain_less_loop = []
for row in rainfall:
	if row[1] < 50:
		rain_less_loop.append(row[0])
print rain_less_loop

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS
