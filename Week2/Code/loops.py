#!/user/bin/env python

""" Some simple loops printing strings, dictionaries, and numbers."""

# Docstrings are part of the running code - other comments are ignored
# thus, they can be accessed during run time

__author__ = 'Michael Schmutzer (michael.schmutzer12@ic.ac.uk)'
__version__ = '0.0.1'

for i in range(5): # prints 0-4
	print i
	
my_list = [0, 2, "geronimo!", 3.0, True, False]
for k in my_list: # prints content of my_list
	print k
	
total = 0
summands = [0, 1, 11, 111, 1111]
for s in summands: # adds 0 to each number in summands, prints outcome.
	print total + s
	
z = 0 
while z < 100: # prints 1 to 100 on a separate line
	z = z + 1
	print (z)
	
b = True
while b:
	print "GERONIMO! infinite loop! ctrl+c to stop!"
