#!/user/bin/env python

"""Regular expression that can recognize a date in the format YYYYMMDD."""

# Docstrings are part of the running code - other comments are ignored
# thus, they can be accessed during run time

__author__ = 'Michael Schmutzer (michael.schmutzer12@ic.ac.uk)'
__version__ = '0.0.1'

import re
re.search(r'^(19|[2-9]\d)\d{2}[01]\d[0-3]\d$', "29930816").group()

