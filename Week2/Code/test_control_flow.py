#!/usr/bin/python

"""Some functions that demonstrate the use of control flows. Docstring tests."""

__author__ = 'Michael Schmutzer (michael.schmutzer12@ic.ac.uk)'
__version__ = '0.0.1'

# Imports

import sys # module to interface program with the operating system
import doctest # Import doctest module

def even_or_odd(x = 0): #if not specified, x should be 0
	
	"""Find whether a number x is even or odd.
	
	>>> even_or_odd(10)
	'10 is Even!'
	
	>>> even_or_odd(5)
	'5 is Odd!'
	
	whenever a float is provided, then the closest integer is used:
	>>> even_or_odd(3.2)
	'3 is Odd!'
	
	in case of negative numbers, the positive is taken:
	>>> even_or_odd(-2)
	'-2 is Even!'
	
	"""
	# Define function to be tested
	if x % 2 == 0: # Conditional if, the % gives the remainder upon \division
		return "%d is Even!" % x
	return "%d is Odd!" % x

# The following is suppressed...it is not being tested
	
#def main(argv):
	#sys.exit #(don't want to do that right now!")
#	print even_or_odd(22)
#	print even_or_odd(33)
#	return 0
	
#if (__name__ == "__main__"):
#	status = main(sys.argv)
#	sys.exit(status)
doctest.testmod() # To run with embedded tests
