#!/user/bin/env python
"""Reads .csv file, prints each row, and prints species name. Extracts 
species name and body mass, and saves them in a seperate file."""

__author__ = 'Michael Schmutzer (michael.schmutzer12@ic.ac.uk)'
__version__ = '0.0.1'

import csv # Import module for dealing with .csv files

# Read a file containing:
# 'Species','Infraorder','Family','Distribution','Body mass male (Kg)'
f = open('../Sandbox/testcsv.csv','rb')
csvread = csv.reader(f)
temp = []
for row in csvread:
	temp.append(tuple(row))
	print row
	print "The species is", row[0]
	
f.close()

# write a file conatining only species name and body mass
f = open('../Sandbox/testcsv.csv', 'rb') # Need to reopen file each time it is read
g = open('../Sandbox/bodymass.csv','wb')

csvread = csv.reader(f)
csvwrite = csv.writer(g)
for row in csvread:
	print row
	csvwrite.writerow([row[0], row[4]])
	
f.close()
g.close()
