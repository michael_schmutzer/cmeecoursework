#!/user/bin/env python

"""Shows how sys.argv stores argument variables."""

__author__ = 'Michael Schmutzer (michael.schmutzer12@ic.ac.uk)'
__version__ = '0.0.1'

import sys
print "Name of the script is: ", sys.argv[0]
print "Number of arguments are: ", len(sys.argv)
print "The arguments are: " , str(sys.argv)
