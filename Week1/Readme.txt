Week 1 contains dummy data & files (./Sandbox) and simple shell scripts
in bash, next to an example Latex script.  

Bash scripts
-----------------
./Code/boilerplate.sh 			# Prints "This is a shell script!"
./Code/CompileLaTeX.sh 			# Compiles pdf from Latex script. Removes auxiliary files
./Code/ConcatenateTwoFiles.sh	# Joins 2 files into a new file. 
./Code/CountLines.sh 			# Counts number of lines within a file
./Code/csvtospace.sh 			# Replaces commas with spaces in .csv files in ./Data/, saves under new name
./Code/MyExampleScript.sh 		# Greets user
./Code/tabtocsv.sh 				# Replaces tabs with commas
./Code/UnixPrac1.txt			# For .fasta files. Returns file/sequence length, search for ATGC, calculate AT/GC ratio
./Code/variables.sh 			# Returns input string. Asks and adds two numbers. 

Latex script
------------------
./Code/Latex/FirstExample.tex # Script for producing .pdf file
