#!bin\bash
#Script: csvtospace.sh
#Author: ms9212 michael.schmutzer@ic.ac.uk
#Desc: Modifies files in ../Data. Creates a *Temperature.txt 
#file from a .csv. Exchanges comma with a space. 
#Date: 9/10/2015

echo "Converting $1 to new file $(basename $1 .csv).txt"

cat ../Data/$1 | tr "," " " > ../Data/$(basename $1 .csv)Temperature.txt

echo "Done!"

exit
