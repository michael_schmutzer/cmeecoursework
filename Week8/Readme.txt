Week 8
------------

This week contains genomic analysis workflows using admixture (plink, ruby)
and R. Admixture analysis is contained in ./ME_Analysis/, repeating the same 
four steps on three different datasets. 

Scripts in ./Code/
--------------------------------
Ancestral_traits.R				# Reconstruct anolis ancestral traits
Phylogenetic_comparison.R		# Regression of primate body mass and gestation time with various phylogenetic corrections
Tree_manipulation.R				# Basic tree manipulation in R. Saves .nex file in ./Results

