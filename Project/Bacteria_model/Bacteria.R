#!/usr/bin/env R
# author: Michael Schmutzer (ms9212@ic.ac.uk)
# version: 0.0.2 

#############################################
# Bacterial population model with selection #
#############################################

# Model assumptions:

#       Reproduction through binary fission, reproduction occurs randomly
#       Constant population size (unrealistic!!)
#       Random replacement of individuals with offspring from another
#       Non-homologous recombination between individual loci, random replacement
#       Recombination can be log-distance dependent
#       Mutations accumulate over time, even if cells are not dividing
#       Addititive  relation between loci & phenotypic trait
#       Selection acts only on the relative probability of reproduction
#       The number of reproducing individuals at each iteration is fixed
#       (this weakens selection!)... selection cannot bring population
#       to extinction

# Some additions:
# More phenotype axes
# Genetic covariance (indep./interdep. selection)
# Moving fitness optima (away from each other/ together)
# Single/bimodal (more?) optima

# PCA of whole array?

# This is a qualitative model... what kind of signatures to look for? Genetic level?

# For now, follow only a single trait with locus-specific recombination!

############################################
############################################

library(cluster)
library(GMD)
library(clValid)

############################################

# PARAMETERS

# Number of iterations per simulation
iter <- 8000
# Total number of individuals
nind <- 2000
# Number of loci contributing to trait (make variable later!)
nloci <- 10
# Mutation rate
mut <- 0.005
# Variance in the effect size of mutations
var_mut <- 0.0001
# Recombination rate (Same for all loci!)
recom <- 0.0001
# Proportion of reproducing cells per iteration (can be no more than 0.5)
# as new cells must replace preexisting ones
repr <- 0.05
# Initial genetic variance
ini_gen_var <- 0.01
# Environmental (fitness) optimum
env_opt <- 1
# Rigour of selection (the smaller the value, the greater the pressure)
s_coef <- 0.001

############################################

# POPULATION ARRAY

# Array of bacterial population, each row is a time step (iteration)
# in the simulation, each column is a slot for an individual. Each layer
# in the array represents a locus, which is summed in a seperate matrix
# which represents the expressed phenotype.
# For a model with two or more initial populations, the array holds
# an additional layer which encodes original population membership, and 
# does not recombine. This could be thought of as a neutral and fixed marker.

pop <- array(data = NA, dim = c(iter, nind, nloci + 1))

# Expressed phenotype (to come under selection)
pheno <- matrix(data = NA, nrow = iter, ncol = nind)

# Vectors to store summary statistics in
# A) The number of clusters
clust_no <- rep(NA, iter)

# The following only make sense with kmeans clustering
# B) Within cluster sum of squares
#wthinss <- rep(NA, iter)
# C) Between cluster sum of squares
#btwss <- rep(NA, iter) 


############################################

# FUNCTIONS

binary_fission <- function(matrix, nind, rate, selection = FALSE, fit_max, population, sel_coef) {
  # Simulates clonal reproduction through random replacement of individuals with
  # the offspring of another. If the option selection == TRUE, individuals with a 
  # phenotype closer to the environmental optimum are preferentially picked to 
  # reproduce.
  
  if (selection == TRUE) {
    # Probability weights for reproduction
    fitness <- exp(- ((population - fit_max) ^ 2) / (2 * sel_coef))
  } else {
    fitness <- rep(1, times = nind) 
  }
  div <- sample(1:nind, ceiling(rate * nind), prob = fitness) # NB: forcing of integers using ceiling 
  dead <- sample(setdiff(1:nind, div), length(div)) # cells to be replaced
  matrix[dead, ] <- matrix[div, ]
  return(matrix)
}

recombination <- function(locus, rate, distance) {
  # Models homologous recombination. Randomly replaces an allele at one locus 
  # with that from another cell. Optionally includes log-distance relation,
  # i.e. the rate of recombination falls with increasing genetic distance.
  
  if (distance == TRUE) {
    # Distance-dependent recombination
    source <- runif(nind) < rate # Randomly choose cells to recombine from
    if (any(source == TRUE)) {
      target <- sample(which(source == FALSE), sum(source))
      gen_distances <- abs(locus[target] - locus[source]) # How different are the loci?
      real_reco <- runif(1:sum(source)) < exp(-gen_distances) # Resample
      locus[target][real_reco] <- locus[source][real_reco]
    }
    
  } else {
    # Distance-independent recombination
    source <- runif(nind) < rate # Randomly choose cells to recombine from
    if (any(source == TRUE)) {
      target <- sample(which(source == FALSE), sum(source))
      locus[target] <- locus[source]
    }
  }
  return(locus)
}

genome_recomb <- function(matrix, rate, distance) {
  # Wrapper for the recombination function. Applies recombination to 
  # each locus seperately.
  matrix <- apply(matrix, 2, recombination, rate, distance)
  return(matrix)
}

mutation <- function(matrix, rate, variance) {
  # Random change in gene effect according to a normal distribution
  # (most changes are small).
  mutants <- runif(nind * nloci) < rate
  matrix[mutants] <- rnorm(sum(mutants), matrix[mutants], sqrt(variance))
  return(matrix)
}

optimum_cluster <- function(cluster_max, dist_matrix, population) {
  # Uses silhouettes to determine the optimum number of clusters for 
  # a particular model iteration. This version is specific for k-means.
  sil <- numeric(cluster_max)
  for (i in 2:cluster_max) {
    clusters <- kmeans(population, i)$cluster
    sil[i] <- summary(silhouette(clusters, dist_matrix))$avg.width
  }
  optimum <- which.max(sil)
  return(optimum)
}

h_optimum_cluster <- function(cluster_max, dist_matrix) {
  # Uses Dunn's index to determine the optimum number of clusters for 
  # a particular model iteration. This version is specific for
  # hierarchical agglomeration.
  dunns <- numeric(cluster_max)
  cluster_tree <- hclust(dist_matrix, method = "single")
  for (i in 2:cluster_max) {
    clusters <- cutree(cluster_tree, k = i)
    dunns[i] <- dunn(dist_matrix, clusters)
  }
  optimum <- which.max(dunns)
  return(optimum)
}

h_elbow_optimum_cluster <- function(cluster_max, dist_matrix) {
  # Uses the "elbow" method to determine the optimum number of clusters
  # This version is specific for hierarchical agglomeration.
  #browser()
  h_css <- css.hclust(dist_matrix, 
                      hclust.FUN.MoreArgs = list(method = "single"),
                      k = cluster_max)
  optimum <- elbow(h_css, 0.01, 0.001)$k
  return(optimum)
}

kmeans_clustering <- function(population, cluster_max = 20) {
  # Clusters individuals using k-means. Uses optimum_cluster
  # to decide the number of clusters at a given iteration.
  
  # Need to take into account that, for certain iterations, 
  # there will be very few distinct datapoints (i.e. beginning
  # of a simulation starting with a homogenous population!)
  
  # How many distinct individuals are there?
  diffr <- sum(duplicated(population) == FALSE)
  # Find distance matrix
  gen_dist <- dist(population)
  # Find optimum number of clusters
  if (diffr == 1) {
    opti <- 1
  } else if (diffr < cluster_max) {
    opti <- optimum_cluster(diffr, gen_dist, population)
  } else {
    opti <- optimum_cluster(cluster_max, gen_dist, population)
  }
  # clustering
  clust <- kmeans(population, opti)
  return(clust)
}

hierarch_clustering <- function(population, cluster_max = 20) {
  # Find distance matrix
  pca <- prcomp(population)
  gen_dist <- dist(pca$x[ , 1:2])
  # How many distinct individuals are there?
  diffr <- sum(duplicated(population) == FALSE)
  # Find optimum
  if (diffr == 1) {
    opti <- 1
  } else if (diffr < cluster_max) {
    opti <- h_optimum_cluster(diffr, gen_dist)
  } else {
    opti <- h_optimum_cluster(cluster_max, gen_dist)
  }
  # clustering
  clust_tree <- hclust(gen_dist, method = "single")
  clust <- cutree(clust_tree, k = opti)
  return(clust)
}

############################################

# SIMULATION

# Initialize population

# Draw each individual locus from a random distribution
# pop[1, , ] <- rnorm(nind * nloci, 0, sqrt(ini_gen_var))
# Alternatively, let them all start at the same value & diverge!
# pop[1, , ] <- 0
# Or, start with two divergent populations...
pop[1, 1:(0.5 * nind), 1:nloci] <- 0
pop[1, (0.5 * nind + 1):nind, 1:nloci] <- 0.2
# Label the initial populations A & B
pop[1, 1:(0.5 * nind), 1 + nloci] <- 0
pop[1, (0.5 * nind + 1):nind, 1 + nloci] <- 1

# Initialize phenotype matrix
pheno[1, ] <- rowSums(pop[1, , 1:nloci ])

############################################

# MODEL RUN

txtbar <- txtProgressBar(2, iter)
for (i in 2:iter) {
  if (i >= 2000) { # Selection kicks in after 2000 iterations
    sweep <- TRUE
  } else {
    sweep <- FALSE
  }
  # Population processes
  pop[i, , ] <- pop[i-1, , ]
  pheno[i, ] <- rowSums(pop[i, , 1:nloci ]) # Phenotype - for selection!
  # Reproduction with selection
  pop[i, , ] <- binary_fission(pop[i, , ], nind, repr, sweep,
                                   env_opt, pheno[i, ], s_coef)
  pop[i, , 1:nloci ] <- genome_recomb(pop[i, , 1:nloci], recom, TRUE) # Recombination
  pop[i, , 1:nloci ] <- mutation(pop[i, , 1:nloci], mut, var_mut)
 
   # Record no. clusters, within/between cluster variability
#  clustered <- hierarch_clustering(pop[i, , ])
  
  # Only use the following lines of code when returning
  # output from kmeans-clustering
#  clust_no[i] <- length(unique(clustered))
#  wthinss[i] <- clustered$tot.withinss
#  btwss[i] <- clustered$betweenss
  
  # Set progress bar!
  setTxtProgressBar(txtbar, i)
}
close(txtbar)

############################################

# PLOTTING 

# Record average & variance of phenotypic trait per iteration

# Average phenotypic trait value
avr_pheno <- rowMeans(pheno)
# Variance in phenotype
var_pheno <- apply(pheno, 1, var)
  
par(mfrow = c(3, 1))

plot(avr_pheno, type = "l", col = "black", ylab = "Mean trait value",
     xlab = "Iteration", main = "Phenotypic trait value")
plot(var_pheno, col = "red", type = "l", ylab = "Variation in trait value",
     xlab = "Iteration", main = "Phenotypic trait value")
plot(rowSums(pop[ , , 1 + nloci]) / nind, type = "l", col = "darkgreen",
     xlab = "Iteration", ylab = "Proportion", 
     main = "Proportion of population 1")

# If using clustering:

# plot(clust_no, type = "l", col = "darkgreen", ylab = "count", xlab = "Time", 
#     main = "Number of clusters")
#plot(wthinss, type = "l", col = "blue", ylab = " Sum of squares", xlab = "Time",
#     main = "Variability within / between clusters",
#     ylim = range(wthinss, btwss, na.rm = TRUE))
#lines(btwss, col = "yellow")


# Alternative way of counting diversity... how many individuals are
# genetically distinct?
#unique_ind <- sum(duplicated(pop[8000, ,]) == FALSE)

# PCA to project population structure onto two axes

quartz() # New window!

par(mfrow = c(3,2))

pca1 <- prcomp(pop[2000, , 1:nloci ])
pca2 <- prcomp(pop[2025, , 1:nloci ])
pca3 <- prcomp(pop[2050, , 1:nloci ])
pca4 <- prcomp(pop[2075, , 1:nloci ])
pca5 <- prcomp(pop[2100, , 1:nloci ])
pca6 <- prcomp(pop[8000, , 1:nloci ])

plot(pca1$x[ , 1], pca1$x[ , 2], 
     col = c("red", "black")[pop[2000, , 1 + nloci] + 1],
     main = "Iteration: 2000")
plot(pca2$x[ , 1], pca2$x[ , 2], 
     col = c("red", "black")[pop[2025, , 1 + nloci] + 1],
     main = "Iteration: 2025")
plot(pca3$x[ , 1], pca3$x[ , 2], 
     col = c("red", "black")[pop[2050, , 1 + nloci] + 1],
     main = "Iteration: 2050")
plot(pca4$x[ , 1], pca4$x[ , 2], 
     col = c("red", "black")[pop[2075, , 1 + nloci] + 1],
     main = "Iteration: 2075")
plot(pca5$x[ , 1], pca5$x[ , 2], 
     col = c("red", "black")[pop[2100, , 1 + nloci] + 1],
     main = "Iteration: 2100")
plot(pca6$x[ , 1], pca6$x[ , 2], 
     col = c("red", "black")[pop[8000, , 1 + nloci] + 1],
     main = "Iteration: 8000")

