%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------
\documentclass[
12pt, % Main font size
a4paper,
oneside
]{article}

\usepackage[margin = 2cm]{geometry}
\usepackage{setspace} % for double spaced lines
\usepackage{lineno} % for line numbers
\usepackage{times}
\usepackage[hang]{footmisc} % Control footer
\usepackage{pgfgantt} % gantt diagram


% Harvard style referencing
\usepackage[comma]{natbib} 
\bibliographystyle{agsm}
\setcitestyle{authoryear, open = {(}, close = {)}}

\doublespacing


%----------------------------------------------------------------------------------------
%	TITLE AND AUTHOR
%----------------------------------------------------------------------------------------

\title{\vfill\textsc{Simulating Selection in Bacterial Speciation}}

\author{Michael Schmutzer}

\date{}

%----------------------------------------------------------------------------------------

\begin{document}

\maketitle

\begin{center}

{\large \textsc{Supervisor}} \\
Timothy Barraclough 
\footnote[1]{
Department of Life Sciences \\
Imperial College London \\
(0)20 7594 2247 \\
t.barraclough@imperial.ac.uk \\
}

\vspace{12 pt}

{\large \textsc{Project Committee}} \\
Tom Bell \\ 
Austin Burt

\end{center}

\vfill

\newpage

%----------------------------------------------------------------------------------------
%	INTRODUCTION
%----------------------------------------------------------------------------------------

\section{Introduction}
  \begin{linenumbers}
  
Modelling is an important tool for exploring the mechanisms of speciation. Most simulation work has been done in obligately sexual organisms, especially with regards to the selective and genetic mechanisms that can overcome the homogenizing effect of recombination, or sex \citep{gavrilets_models_2014}. Much less attention has been granted to asexual (clonal) organisms, although they form a considerable challenge to traditional conceptions of species \citep{doolittle_microbial_2008}, and careful simulation studies are needed to account for this \citep{fraser_bacterial_2009}.

In addition, the population genetics of asexual organisms such as bacteria differ substantially. For example, recombination in bacteria (i) tends to be much rarer (ii) replaces rather than shuffles alleles and (iii) occurs over genetic distances far greater than those between reproductively isolated, multicellular eukaryotes \citep{doolittle_microbial_2008}. As a consequence, evolutionary independence of bacterial lineages often appears more transient than that of obligately sexual organisms, and the pattern of genetic diversity resembles more that of a network than of a hierarchically nested tree.

However, bacterial lineages do reach evolutionary independence, and form recognizable ecological and genetic units \citep{hanage_modelling_2006, fraser_bacterial_2009}. In simulation, divergent selection can overcome the homogenizing effect of recombination, and the spread of globally adaptive mutations \citep{majewski_adapt_1999}. In fact, simulations of neutrally evolving bacteria, with or without recombination, show the formation of distinctive clustering of individuals, which is favoured even more when the rate of recombination falls with increasing genetic distance \citep{hanage_modelling_2006}.

Given that genetic clusters of bacteria can arise under neutral conditions \citep{hanage_modelling_2006}, selection may seem to be unnecessary for bacterial `speciation'. However, we know from empirical studies that much of the bacterial genome is under selection, and many bacterial `species' do conform to a specific ecotype \citep{fraser_bacterial_2009}. It is currently not clear if and how speciation in bacteria under selection differs from that under neutrality. Will the pattern of genetic diversity change under selection? What is the effect of different kinds of selection?

 \end{linenumbers}
%----------------------------------------------------------------------------------------
%	PROPOSED METHODS
%----------------------------------------------------------------------------------------

\section{Proposed Methods}
  \begin{linenumbers}

The project is to start with a more mechanistic version of the model of \citet{fraser_bacterial_2009}, that is, a model with mutation, recombination, and a fixed population size (i.e. drift), but wherein the recombination rate is dependent upon the genetic distance between alleles. The construction of an explicitly biological model is to eventually invite comparison to empirical data of genetic variation in bacteria. To this model would be added either uniform or divergent selection regimes, and the resulting variability (either in the degree of clustering, or the effective population size) compared across treatments. Subject to time constraints, the model can then be extended to involve greater variability in selection, for example in the form of of environmental heterogenicity (as gradients or patchiness). Alternatively, the model may include selection on multiple genes and in different directions (e.g. bacterial genomes are known to consist of housekeeping and locally adaptive genes). Also, the mechanism underpinning the relation between recombination and genetic distance could be altered. Once completed, runs with varying parameter values will be submitted to Imperial's HPC cluster.

  \end{linenumbers}
%----------------------------------------------------------------------------------------
%	PROJECT FEASABILITY
%----------------------------------------------------------------------------------------

\section{Project Feasability}
  \begin{linenumbers}
  
There are several possible approaches to building the kind of model outlined above. One possibility is to adapt an existing Fisher-Wright model (with selection) on sexual diploids to the specifics of bacterial reproduction. Such a model can be computationally intensive, but is also flexible and easily extended. Although this will necessitate modelling fairly small populations, it should be kept in mind that bacterial populations, although often vast, have small effective population sizes \citet{fraser_bacterial_2009}. Alternatively, a possibly more analytical approach would be to attempt modeling diversity on a population level.

  \end{linenumbers}

%----------------------------------------------------------------------------------------
% Gantt chart

\begin{figure}[b]
  \begin{center}

\begin{ganttchart}[
    hgrid,                                % Horizontal grid lines
    vgrid,                                % Vertical grid lines
    x unit = .55 cm,                       % Size of block
    bar/.append style = {fill = black}    % Black bars
  ]{1}{20}
 
 %---------------------- Title months
  \gantttitle{April}{4}
  \gantttitle{May}{4}
  \gantttitle{June}{4}
  \gantttitle{July}{4}
  \gantttitle{August}{4}
  \\
  
  %-------------------- Task bars
  \ganttbar{Build basic model}{1}{8} \\
  \ganttbar{Incorporate selection}{6}{10} \\
  \ganttbar{Add env/genetic complexity}{10}{14} \\
  
  \ganttmilestone{Working code}{14} \\
  
  \ganttbar{HPC simulation}{15}{18} \\
  \ganttbar{Data interpretation \& write up}{16}{20}
  
  %-------------------- Link bars & milestones
  \ganttlink{elem0}{elem3}
  \ganttlink{elem1}{elem3}
  \ganttlink{elem2}{elem3}
  \ganttlink{elem3}{elem4}
  \ganttlink{elem4}{elem5}
  
  
\end{ganttchart}

\end{center}
\end{figure}


%----------------------------------------------------------------------------------------
% REFERENCES
%----------------------------------------------------------------------------------------

\bibliography{Proposal.bib} 

%----------------------------------------------------------------------------------------

\end{document}