Starting weekly assessment for Michael Adriaan Constantijn Marie, Week4

Current Marks = 100

Note that: 
(1) Major sections begin with a double "====" line 
(2) Subsections begin with a single "====" line 
(3) Code output or text file content are printed within single "*****" lines 

======================================================================
======================================================================
PART 1: Checking project workflow...

Found the following directories in parent directory: .git, Week2, Week9, Week4, Assessment, Week1, Week5, Week8, Week6

Found the following files in parent directory: README.txt, .gitignore

Checking for key files in parent directory...

Found .gitignore in parent directory, great! 

Printing contents of .gitignore:
**********************************************************************
*~ 
*.tmp
**********************************************************************

Found README in parent directory, named: README.txt

Printing contents of README.txt:
**********************************************************************
My CMEE 2015-16 Coursework Repository
Do I like this better?
**********************************************************************

======================================================================
Looking for the weekly directories...

Found 7 weekly directories: Week1, Week2, Week4, Week5, Week6, Week8, Week9

The Week4 directory will be assessed 

======================================================================
======================================================================
PART 2: Checking weekly code and workflow...

======================================================================
Assessing WEEK4...

Found the following directories: RCode, Practicals, Code, Data, Results

Found the following files: Practical_5.pdf, Practical_2.pdf, Practical_1.pdf, Practical_8.pdf, Practical_7.pdf, Practical_3.pdf, Practical_6.pdf, Readme.txt, Practical_4.pdf

Checking for readme file in weekly directory...

Found README in parent directory, named: Readme.txt

Printing contents of Readme.txt:
**********************************************************************
Week4 contains worked examples of statistical tests in R, closely following
instructions saved in ./Practicals. Sample data is in ./Data, ./Output 
contains graphics produced in R.

R scripts in ./Code/			Topics
---------------------------
Practical1.R					# Data types & manipulation
Practical2.R					# Data frames, input / output 
Practical3.R					# Base plots
Practical4.R					# t-test & chi-squared test
Practical5.R					# t & Wilcoxon tests, correlation 
Practical6.R					# ANOVA & linear regression
Practical7.R					# Multivariate models
Practical8.R					# Model criticism simplification
**********************************************************************

Found 8 code files: Practical8.R, Practical2.R, Practical7.R, Practical3.R, Practical4.R, Practical1.R, Practical6.R, Practical5.R

Found the following extra files: Rplots.pdf
What are they doing here?! 

0.5 pt deducted per extra file

Current Marks = 99.5

======================================================================
Testing script/code files...

======================================================================
Inspecting script file StudentRepos/MichaelAdriaanConstantijnMarieSchmutzer_MS9212/Week4/RCode/Practical8.R...

File contents are:
**********************************************************************
# Model criticism and simplification

setwd("~/Documents/CMEECourseWork/Week4/Data")

decay <- read.delim("decay.txt")
summary(decay)

plot(mass ~ duration, data = decay,
     xlab = "Burial duration (days)",
     ylab = "Remaining mass (g)")
decayMod <- lm(mass ~ duration, data = decay)
summary(decayMod)
abline(decayMod)

par(mfrow = c(2, 2))
plot(decayMod)

# Residual patterned, non-normal distribution,
# many points with strong leverate

# Apply a log transformation

decay$logMass <- log10(decay$mass)

par(mfrow = c(1, 1))
plot(logMass ~ duration, data = decay,
    xlab = "Burial duration (days)",
    ylab = "Log remaining mass (g)")
logDecayMod <- lm(logMass ~ duration, data = decay)
summary(logDecayMod)
abline(logDecayMod)

par(mfrow = c(2, 2))
plot(logDecayMod)

birdMass <- read.delim("birdMass.txt")
str(birdMass)

plot(mass ~ species, data = birdMass, par(mfrow = c(1, 1)))
birdMod <- lm(mass ~ species, data = birdMass)
summary(birdMod)
par(mfrow = c(2, 2))
plot(birdMod) # seem to be 2 groups for species A

######## Count data ############

species <- read.delim("species.txt")
par(mfrow = c(1,1))
plot(Species ~ Area, data = species,
     xlab = "Sampling area", 
     ylab = "Number of species")
speciesAreaMod <- lm(Species ~ Area, data = species)
summary(speciesAreaMod)
abline(speciesAreaMod)

par(mfrow = c(2,2))
plot(speciesAreaMod)

# Fitting a species area curve
species$Arealog <- log10(species$Area)
par(mfrow = c(1,1))
plot(Species ~ Arealog, data = species,
     xlab = "Log sampling area", 
     ylab = "Number of species")
speciesArealogMod <- lm(Species ~ Arealog, data = species)
summary(speciesArealogMod)
abline(speciesArealogMod)
par(mfrow = c(2,2))
plot(speciesArealogMod)

############# Model simplification #############

desertRats <- read.delim("desertRats.txt")
str(desertRats)
pairs(desertRats)

maxMod <- lm(rodent ~ rain + predators + cover + seed, data = desertRats)
summary(maxMod)

# Simplify model using update

simpMod1 <- update(maxMod, . ~ . - predators) 
summary(simpMod1)

anova(maxMod, simpMod1)

simpMod2 <- update(simpMod1, . ~ . - cover)
summary(simpMod2)

anova(maxMod, simpMod1, simpMod2)

simpMod3 <- update(simpMod2, . ~ . - seed)
summary(simpMod3)

anova(maxMod, simpMod1, simpMod2, simpMod3)

nullMod <- update(maxMod, . ~ 1)
anova(maxMod, simpMod3, nullMod)

############### Merging levels #############

competition <- read.delim("competition.txt")
plot(biomass ~ clipping, data = competition, 
     ylab = "Plant biomass (g)",
     xlab = "Root clipping treatment")
CfullMod <- lm(biomass ~ clipping, data = competition)
summary(CfullMod)

# Merging levels

competition$clipSimp <- competition$clipping
levels(competition$clipSimp)
levels(competition$clipSimp) <- c("control", "n25", "n50", "r", "r")

CsimpMod <- lm(biomass ~ clipSimp, data = competition)
anova(CfullMod, CsimpMod)

competition$clipSimp2 <- competition$clipping
levels(competition$clipSimp2)
levels(competition$clipSimp2) <- c("control", "n", "n", "r", "r")

CsimpMod2 <- lm(biomass ~ clipSimp2, data = competition)
anova(CfullMod, CsimpMod2)

competition$clipSimp3 <- competition$clipping
levels(competition$clipSimp3)
levels(competition$clipSimp3) <- c("control", "manip", "manip", "manip", "manip")

CsimpMod3 <- lm(biomass ~ clipSimp3, data = competition)
anova(CfullMod, CsimpMod3)

anova(CsimpMod3)
**********************************************************************

Testing Practical8.R...

Output (only first 500 characters): 

**********************************************************************

**********************************************************************

Encountered error:
Error in setwd("~/Documents/CMEECourseWork/Week4/Data") : 
  cannot change working directory
Execution halted

======================================================================
Inspecting script file StudentRepos/MichaelAdriaanConstantijnMarieSchmutzer_MS9212/Week4/RCode/Practical2.R...

File contents are:
**********************************************************************
# Practical 2 - Importing data

# Making some sequences

1:10
-4:4
12:3

# In steps of 2...
seq(from = 1, to = 20, by = 2)
seq(from = 1, to = 3, length = 10)

# Repeat values in a vector
rep(x = 1:4, times = 2)
rep(x = 1:4, each = 2)
rep(x = 1:4, times = 1:4)

# Factors
myCharacterVector <- c("low","low", "low", "low"
           , "high", "high", "high","high")
myFactor <- as.factor(myCharacterVector)
str(myFactor)

# Generating levels in a factor using gl()
myFactor <- gl(n = 2, k = 4)
print(myFactor)

myFactor <- gl(n = 2, k = 2, length = 8, labels = c("low", "high"))
print(myFactor)

############# Data frame ####################

# Data vectors
MyNumerics <- c(1.3, 2.5, 1.9, 3.4, 5.6, 1.4, 3.1, 2.9)
MyCharacters <- gl(n = 2, k = 2, length = 8, labels = c("low", "high"))
MyLogicals <- c(T,T,F,F,T,T,F,F)

# Assemble a dataframe
MyData <- data.frame(num = MyNumerics, char = MyCharacters, logical = MyLogicals)
print(MyData)
str(MyData)

# Keep characters as strings, not factors
MyData <- data.frame(num = MyNumerics, char = MyCharacters, logical = MyLogicals, stringsAsFactors = FALSE)
str(MyData)
print(MyData)

# Reading input from .csv file

setwd("~/Documents/CMEECourseWork/Week4/Data")
factories <- read.csv("factories.csv")
str(factories)

factories$rate
# using logical test

factories$factory == "Sheffield"
factories$rate[factories$factory == "Sheffield"]
factories$treatment == "C"
factories$rate[factories$treatment == "C"]

# Creating a new column

factories$country <- "England"
factories$country[factories$factory == "Gwent"] <- "Wales"
factories$country[factories$factory == "Newport"] <- "Wales"
str(factories)

# attach() commands
attach(factories)
rate[treatment == "C"]
rate[factory == "Sheffield"]
detach(factories)

with(factories, rate[factory == "Sheffield"])


# Using subset

WelshData <- subset(factories, subset = country == "Wales")
str(WelshData)

WelshData <- subset(factories, subset = country == "Wales", select = rate)
str(WelshData)
**********************************************************************

Testing Practical2.R...

Output (only first 500 characters): 

**********************************************************************
 [1]  1  2  3  4  5  6  7  8  9 10
[1] -4 -3 -2 -1  0  1  2  3  4
 [1] 12 11 10  9  8  7  6  5  4  3
 [1]  1  3  5  7  9 11 13 15 17 19
 [1] 1.000000 1.222222 1.444444 1.666667 1.888889 2.111111 2.333333 2.555556
 [9] 2.777778 3.000000
[1] 1 2 3 4 1 2 3 4
[1] 1 1 2 2 3 3 4 4
 [1] 1 2 2 3 3 3 4 4 4 4
 Factor w/ 2 levels "high","low": 2 2 2 2 1 1 1 1
[1] 1 1 1 1 2 2 2 2
Levels: 1 2
[1] low  low  high high low  low  high high
Levels: low high
  num char logical
1 1.3  low    TRUE
2 2.5  low    TRUE

**********************************************************************

Encountered error:
Error in setwd("~/Documents/CMEECourseWork/Week4/Data") : 
  cannot change working directory
Execution halted

======================================================================
Inspecting script file StudentRepos/MichaelAdriaanConstantijnMarieSchmutzer_MS9212/Week4/RCode/Practical7.R...

File contents are:
**********************************************************************
# Multivariate models

setwd("~/Documents/CMEECourseWork/Week4/Data")

daphnia <- read.delim("daphnia.txt")
summary(daphnia)

par(mfrow = c(1, 2))
plot(Growth.rate ~ Detergent, data = daphnia)
plot(Growth.rate ~ Daphnia, data = daphnia)

seFun <- function(x){
  sqrt(var(x)/length(x))
}

detergentMean <- with(daphnia, tapply(Growth.rate, INDEX = Detergent,
                                      FUN = mean))
detergentSEM <- with(daphnia, tapply(Growth.rate, INDEX = Detergent,
                                     FUN = seFun))
cloneMean <- with(daphnia, tapply(Growth.rate, INDEX = Daphnia, 
                                  FUN = mean))
cloneSEM <- with(daphnia, tapply(Growth.rate, INDEX = Daphnia, FUN = seFun))

par(mfrow = c(2,1), mar = c(4, 4, 1, 1))
barMids <- barplot(detergentMean,
                  xlab = "Detergent type",
                  ylab = "Population growth rate",
                  ylim = c(0, 5))
arrows(barMids, detergentMean - detergentSEM, barMids, 
      detergentMean + detergentSEM, code = 3, angle = 90)

barMids <- barplot(cloneMean, xlab = "Daphnia clone",
                  ylab = "Population growth rate",
                  ylim = c(0, 5))
arrows(barMids, cloneMean - cloneSEM, barMids, cloneMean + cloneSEM,
       code =3, angle = 90)

# Making a model to check for additive effects

daphniaMod <- aov(Growth.rate ~ Detergent + Daphnia, data = daphnia)
summary(daphniaMod)
summary.lm(daphniaMod)

detergentMean - detergentMean[1]

# Tukey's HSD
daphniaModHSD <- TukeyHSD(daphniaMod)
daphniaModHSD

par(mfrow = c(1,2), mar = c(5, 8, 2, 2), las = 1)
plot(daphniaModHSD)

# Multiple regression

timber <- read.delim("timber.txt")
summary(timber)

# Exploratory plots

pairs(timber)
cor(timber)

# Model fitting, is simplification justified?

timberMod <- lm(volume ~ girth + height, data = timber)
summary.aov(timberMod)
summary(timberMod)

################# Analysis of Covariance ##################

plantGrowth <- read.delim("ipomopsis.txt")
summary(plantGrowth)

# Model fitting

plantMod <- lm(Fruit ~ Root + Grazing, data = plantGrowth)
anova(plantMod)
summary(plantMod)

# Plot model

par(mfrow = c(1,1))

plantGrowth$plotChar <- 1
plantGrowth$plotChar[plantGrowth$Grazing == "Ungrazed"] <- 19
plantGrowth$plotCol <- "red"
plantGrowth$plotCol[plantGrowth$Grazing == "Ungrazed"] <- "darkblue"

plot(Fruit ~ Root, data = plantGrowth, 
     ylab = "Fruit production (dry weight, mg)",
     xlab = "Initial root diameter (mm)",
     pch = plotChar,
     col = plotCol)

abline(a = -127.829, b = 23.56, col = "red")
abline(a = -127.829 + 36.103, b = 23.56, col = "blue")
**********************************************************************

Testing Practical7.R...

Output (only first 500 characters): 

**********************************************************************

**********************************************************************

Encountered error:
Error in setwd("~/Documents/CMEECourseWork/Week4/Data") : 
  cannot change working directory
Execution halted

======================================================================
Inspecting script file StudentRepos/MichaelAdriaanConstantijnMarieSchmutzer_MS9212/Week4/RCode/Practical3.R...

File contents are:
**********************************************************************
# Plotting

setwd("~/Documents/CMEECourseWork/Week4/Data")

plantGrowth <- read.delim("ipomopsis.txt")
str(plantGrowth)

# Start with plotting data 

hist(plantGrowth$Fruit)

par(mfrow = c(2,1), mar = c(4,4,1,1))
hist(plantGrowth$Fruit, xlab = "Fruit production (dry weight, mg)", main = "")
hist(plantGrowth$Fruit, xlab = "Fruit production (dry weight, mg)", main = "", breaks = 10)
# R treats "breaks" argument as a suggestion, 
# supply a vector to specify specific number of breaks

hist(plantGrowth$Fruit, xlab = "Fruit production (dry weight, mg)",
     main = "",
     breaks = seq(0, 125, by = 25))

# Using density rather than frequency

hist(plantGrowth$Fruit, xlab = "Fruit production (dry weight, mg)",
     main = "",
     breaks = seq(0, 125, by = 25),
     freq = FALSE)

# Box and Whiskers

plot(x = plantGrowth$Grazing, y = plantGrowth$Fruit)
plot(Fruit ~ Grazing, data = plantGrowth,
     xlab = "Rabbit grazing treatment",
     ylab = "Fruit production (dry weight, mg)",
     main = "")

# Scatterplots

plot(Fruit ~ Root, data = plantGrowth,
     ylab = "Fruit production (dry weight, mg)",
     xlab = "Initial root diameter (mm)",
     pch = 19)

# Tedious way of setting up 2 character plots

plot(Fruit ~ Root, data = plantGrowth, subset = Grazing == "Grazed",
     ylab = "Fruit production (dry weight, mg)",
     xlab = "Initial root diameter (mm)",
     pch = 1)
points(Fruit ~ Root, data = plantGrowth, subset = Grazing == "Ungrazed",
       pch = 19)

xRange <- range(plantGrowth$Root) # Set up the full range
plot(Fruit ~ Root, data = plantGrowth, subset = Grazing == "Grazed",
     ylab = "Fruit production (dry weight, mg)",
     xlab = "Initial root diameter (mm)",
     xlim = xRange,
     pch = 1)
points(Fruit ~ Root, data = plantGrowth, subset = Grazing == "Ungrazed",
       pch = 19)

# A bit faster!

plantGrowth$plotChar <- 1
plantGrowth$plotChar[plantGrowth$Grazing == "Ungrazed"] <- 19
plot(Fruit ~ Root, data = plantGrowth,
     ylab = "Fruit production (dry weight, mg)",
     xlab = "Initial root diameter (mm)",
     pch = plotChar)

plantGrowth$plotText <- "G" # Letters
plantGrowth$plotText[plantGrowth$Grazing == "Ungrazed"] <- "U"
plot(Fruit ~ Root, data = plantGrowth,
     ylab = "Fruit production (dry weight, mg)",
     xlab = "Initial root diameter (mm)",
     pch = plotText)

plantGrowth$plotCol <- "red" # Colours
plantGrowth$plotCol[plantGrowth$Grazing == "Ungrazed"] <- "darkblue"
plot(Fruit ~ Root, data = plantGrowth,
     ylab = "Fruit production (dry weight, mg)",
     xlab = "Initial root diameter (mm)",
     pch = plotChar,
     col = plotCol)

# Barplots

fruitMean <- with(plantGrowth, tapply(Fruit, Grazing, mean))
fruitSD <- with(plantGrowth, tapply(Fruit, Grazing, sd))

barplot(fruitMean, ylab = "Mean fruit production (dry weight, mg)",
        xlab = "Rabbit grazing treatment")

yLim <- c(0, max(fruitMean + fruitSD))
xMids <- barplot(fruitMean, ylab = "Mean fruit production (dry weight, mg)",
                 xlab = "Rabbit grazing treatment",
                 ylim = yLim,
                 mar = c(5.1, 5.1, 1, 1))
arrows(x0 = xMids,
       y0 = fruitMean - fruitSD, 
       x1 = xMids, 
       y1 = fruitMean + fruitSD,
       ang = 90,
       code = 3)

# Graphic parameter settings
par("mar")
par(mar = c(4.1, 4.1, 1.1, 1.1))

# Save a plot in Results folder

setwd("~/Documents/CMEECourseWork/Week4/Results")

pdf(file = "myPlot.pdf", width = 6, height = 4, pointsize = 16)
plot(1:10)
title(main = "My pdf of some dull numbers")
dev.off()

**********************************************************************

Testing Practical3.R...

Output (only first 500 characters): 

**********************************************************************

**********************************************************************

Encountered error:
Error in setwd("~/Documents/CMEECourseWork/Week4/Data") : 
  cannot change working directory
Execution halted

======================================================================
Inspecting script file StudentRepos/MichaelAdriaanConstantijnMarieSchmutzer_MS9212/Week4/RCode/Practical4.R...

File contents are:
**********************************************************************
# Probability distributions

# Descriptors of a t-distribution

dt(0.5, df = 19) # Density at t = 0.5, i.e. how probable is a difference btw means of 0.5?
pt(0.5, df = 19) # probability of t =< 0.5 (cumulative)
qt(0.8, df = 19) # Given a prob of =< 0.8, what would t be? (cumulative)
# Random sampling from t distribution:
randT <- rt(n = 1000, df = 19)
hist(randT)

# Example t-test ... step by step
x <- c(6, 4, 2, 5, 4, 4, 5, 6, 3, 3, 4, 6, 8, 5, 6, 6, 5, 4, 8, 6)
xN <- length(x)
xMean <- mean(x)
xSS <- sum((x - xMean)^2)
xVar <- xSS/(xN - 1)
xSE <- sqrt(xVar/xN)
xT <- (xMean - 4)/xSE
print(xT)

# A bit simpler

xT2 <- (xMean - 4) / sqrt(var(x)/length(x))
print(xT2)

# Is this significant (p =< 0.05)?

pt(xT, df = 19) # Probability of t =< 2.87 (less extreme)
1 - pt(xT, df = 19) # Probability of t >= 2.87 (or more extreme)
# Make t test 2 tailed (i.e. double prob)
1 - pt(xT, df = 19) + pt(-xT, df = 19) # or
2 * (1 - pt(xT, df = 19))

# Finding confidence limits of the mean (due to error from subsampling)
xTci <- qt(c(0.025, 0.975), df = 19)
print(xTci)

xTci * xSE
xCI <- xMean + (xTci * xSE)
print(xCI)

# The easy way!

t.test(x, mu = 4)

# X squared test
# Make a matrix!

matrix(1:12, ncol = 3)
matrix(1:12, nrow = 2, byrow = TRUE)
matrix(1:12, nrow = 3, dimnames = list(Row  = c("1", "2", "3"), Column = c("A", "B", "C", "D")))

myMat <- matrix(1:12, ncol = 3)
myMat[2, ]
myMat[ , 3]
myMat[3, 2]
myMat[3, 2] <- -99
myMat[, 3] <- 33
myMat

# Chi squared test

hairEyes <- matrix(c(34, 59, 3, 10, 42, 47), ncol = 2, dimnames = list(Hair = c("Black", "Brown", "Blond"), Eyes = c("Brown", "Blue")))
hairEyes

rowTot <- rowSums(hairEyes)
colTot <- colSums(hairEyes)
tabTot <- sum(hairEyes)
Expected <- outer(rowTot, colTot)/tabTot
Expected

cellChi <- (hairEyes - Expected)^2 / Expected
tabChi <- sum(cellChi)
tabChi

1-pchisq(tabChi, df = 2)

hairChi <- chisq.test(hairEyes)
print(hairChi)
str(hairChi)
**********************************************************************

Testing Practical4.R...

Output (only first 500 characters): 

**********************************************************************
[1] 0.3454832
[1] 0.6885918
[1] 0.8609506
[1] 2.874173
[1] 2.874173
[1] 0.995143
[1] 0.004857032
[1] 0.009714064
[1] 0.009714064
[1] -2.093024  2.093024
[1] -0.7282179  0.7282179
[1] 4.271782 5.728218

	One Sample t-test

data:  x
t = 2.8742, df = 19, p-value = 0.009714
alternative hypothesis: true mean is not equal to 4
95 percent confidence interval:
 4.271782 5.728218
sample estimates:
mean of x 
        5 

     [,1] [,2] [,3]
[1,]    1    5    9
[2,]    2    6   10
[3,]    3    7   11
[4,]  
**********************************************************************

Code ran without errors

Time consumed = 0.16779s

======================================================================
Inspecting script file StudentRepos/MichaelAdriaanConstantijnMarieSchmutzer_MS9212/Week4/RCode/Practical1.R...

File contents are:
**********************************************************************
# Practical one

setwd("/home/ms9212/Documents/CMEECourseWork/Week4/RCode/")

# Some basic calculations
2 * 2 + 1
2 * (2 + 1)
12/2^3
(12/2)^3

example(image)

# Pythagoras theorem
x <- 5 
y <- 2

z <- sqrt(x^2 + y^2)
print(z)

# Making some vectors
myNumericVector <- c(1.3, 2.5, 1.9, 3.4, 5.6, 1.4, 3.1, 2.9)
myCharacterVector <- c("low", "low", "low", "low", "high","high","high","high")
myLogialVector <- c(TRUE,TRUE,FALSE,FALSE,TRUE,TRUE,FALSE,FALSE)

str(myNumericVector)
str(myCharacterVector)
str(myLogialVector)

myMixedVector <- c(1, TRUE,FALSE,3,"help", 1.2, TRUE, "notwhatIplanned")
str(myMixedVector)

a <- 1:8
a^2
a * 3
a * c(10, 100)
a * c(10, 100, 1000)

# Clean up all variables
rm(list = ls())
**********************************************************************

Testing Practical1.R...

Output (only first 500 characters): 

**********************************************************************

**********************************************************************

Encountered error:
Error in setwd("/home/ms9212/Documents/CMEECourseWork/Week4/RCode/") : 
  cannot change working directory
Execution halted

======================================================================
Inspecting script file StudentRepos/MichaelAdriaanConstantijnMarieSchmutzer_MS9212/Week4/RCode/Practical6.R...

File contents are:
**********************************************************************
# ANOVA and linear regression

setwd("~/Documents/CMEECourseWork/Week4/Data")

competition <- read.delim("competition.txt")
str(competition)
summary(competition)

plot(biomass ~ clipping, data = competition, 
     ylab = "Plant biomass (g)",
     xlab = "Root clipping treatment")

# Producing analysis of variance model

competModel <- aov(biomass ~ clipping, data = competition)
print(competModel)
summary(competModel) # see if explanatory variable explains
#significant proportion of variation
summary.lm(competModel) # Estimate of coefficients (model estimate of difference btw means)

# Performs t-test on the difference between intercept (control here), and each factor mean
# Demonstration of t-tests for each coefficient
competMean <- with(competition, tapply(biomass, INDEX = clipping,
                                       FUN = mean))
print(competMean)
# Define function returning the standart error
seFun <- function(x) {
  sqrt(var(x)/length(x))
}
competSEMean <- with(competition, tapply(biomass, INDEX = clipping, FUN = seFun))
print(competSEMean)

# Multiple t-tests, correct using Tukey's HSD

competTukeyHSD <- TukeyHSD(competModel)
competTukeyHSD
plot(competTukeyHSD)

# Plotting means & st. errors

midpoints <- barplot(competMean, xlab = "Root competition treatment",
                     ylab = "Plant biomass (g)",
                     ylim = c(0, 700))
arrows(midpoints, competMean - competSEMean, midpoints,
       competMean + competSEMean, code = 3, angle = 90)

# Non-parametric test: Kruskal-Wallis
kruskal.test(biomass ~ clipping, data = competition)

############### Regression ######################

tannin <- read.delim("tannin.txt")
str(tannin)
summary(tannin)
plot(growth ~ tannin, data = tannin,
     xlab = "Tannin concentration",
     ylab = "Growth (mg)")

# fitting a model

tanninMod <- lm(growth ~ tannin, data = tannin)
summary.aov(tanninMod)
summary(tanninMod)
abline(tanninMod)
**********************************************************************

Testing Practical6.R...

Output (only first 500 characters): 

**********************************************************************

**********************************************************************

Encountered error:
Error in setwd("~/Documents/CMEECourseWork/Week4/Data") : 
  cannot change working directory
Execution halted

======================================================================
Inspecting script file StudentRepos/MichaelAdriaanConstantijnMarieSchmutzer_MS9212/Week4/RCode/Practical5.R...

File contents are:
**********************************************************************
# Practical 5

setwd("~/Documents/CMEECourseWork/Week4/Data")

ozoneDat <- read.delim("gardenOzone.txt")
str(ozoneDat)
summary(ozoneDat)

par(mfrow  = c(2, 2))
plot(ozone ~ garden, data = ozoneDat,
     ylab = "[Ozone] (ppm)",
     xlab = "Garden")
with(ozoneDat, hist(ozone[garden == "A"], 
                    main = "Garden A",
                    xlab = "[Ozone] (ppm)"))
with(ozoneDat, hist(ozone[garden == "B"], 
                    main = "Garden B",
                    xlab = "[Ozone] (ppm)"))
with(ozoneDat, hist(ozone[garden == "C"], 
                    main = "Garden C",
                    xlab = "[Ozone] (ppm)"))

# Two sample t test

A <- ozoneDat$ozone[ozoneDat$garden == "A"]
B <- ozoneDat$ozone[ozoneDat$garden == "B"]

t.test(A, B)

# Easier code...

t.test(ozone ~ garden, data = ozoneDat, subset = garden != "C")

# Assuming data is paired

A <- ozoneDat$ozone[ozoneDat$garden == "A"]
B <- ozoneDat$ozone[ozoneDat$garden == "B"]

t.test(A - B)

# Easier code...

t.test(ozone ~ garden, data = ozoneDat, subset = garden != "C", paired = TRUE)

# Wilcoxon test with a bimodal dataset (Garden C)
GardenC <- ozoneDat$ozone[ozoneDat$garden == "C"]
wilcox.test(GardenC, mu = mean(A))

# What is the V statistic?

cWilcox <- data.frame(C = GardenC, diff = GardenC - mean(A))
print(cWilcox)
cWilcox <- subset(cWilcox, subset = diff != 0)
cWilcox$rankAbs <- rank(abs(cWilcox$diff))
print(cWilcox)

# Two sample Wilcoxon test
wilcox.test(ozone ~ garden, data = ozoneDat, subset = garden != "B")


# Correlation

kicksample <- read.delim("kicksample.txt")
str(kicksample)
summary(kicksample)

# Plotting, with x & y means
par(mfrow = c(1,1))
plot(Upstream ~ Downstream, data = kicksample)
with(kicksample, abline(v = mean(Downstream), h = mean(Upstream), lty = 2))
# Significance test of correlation
with(kicksample, cor.test(Upstream, Downstream))
with(kicksample, cor.test(Upstream, Downstream, method = "spearman"))
with(kicksample, cor.test(Upstream, Downstream, method = "kendall"))
**********************************************************************

Testing Practical5.R...

Output (only first 500 characters): 

**********************************************************************

**********************************************************************

Encountered error:
Error in setwd("~/Documents/CMEECourseWork/Week4/Data") : 
  cannot change working directory
Execution halted

======================================================================
======================================================================
Finished running scripts

Ran into 7 errors

======================================================================
======================================================================

FINISHED WEEKLY ASSESSMENT

Current Marks for the Week = 99.5

NOTE THAT THESE ARE NOT THE FINAL MARKS FOR THE WEEK, BUT UPPER BOUND ON THE MARKS!