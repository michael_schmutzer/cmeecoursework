#!/usr/bin/env R
# Author: Michael Schmutzer (ms9212@ic.ac.uk)
# version: 0.0.1

# Return summary of model fits according to r-squared, AIC, and BIC metrics. 

library(knitr)

##----q1----

############################# Analysis of Model fits ##################################

# According to AIC, BIC, & r-squared, which models fitted the data best?

############################ Data preparation #################################

# Read in 
schoolfield_parameters <- read.csv("../Results/schoolfield_estimates.csv")
cubic_parameters <- read.csv("../Results/cubic_estimates.csv")
growth_response <- read.csv("../Data/ThermResp_startvals.csv")

# Subset Schoolfield parameter estimates according to model
schoolf_param_full <- droplevels(schoolfield_parameters[schoolfield_parameters$Model == "Schoolfield_full", ])
schoolf_param_low <- droplevels(schoolfield_parameters[schoolfield_parameters$Model == "Schoolfield_low", ])
schoolf_param_high <- droplevels(schoolfield_parameters[schoolfield_parameters$Model == "Schoolfield_high", ])

# Model pruning, get rid of the worst fits!

schoolf_param_full <- droplevels(schoolf_param_full[schoolf_param_full$R_squared >= 0.5, ])
schoolf_param_low <- droplevels(schoolf_param_low[schoolf_param_low$R_squared >= 0.5, ])
schoolf_param_high <- droplevels(schoolf_param_high[schoolf_param_high$R_squared >= 0.5, ])

###################################### Functions ###############################

find_mode <- function(x) {
  # Takes vector of values, finds the mode
  dens <- density(x)
  return(dens$x[which.max(dens$y)])
}

find_percent <- function(model_data_frame) {
  # Finds percentage of trait responses fitted by the model in model_data_frame
  return(nrow(model_data_frame) / length(Final_ID) * 100)
}

################# Construct count table of best model fits ##########################

Final_ID <<- levels(growth_response$FinalID)

# Vectors to store 
aic_max <- c()
bic_max <- c()
r_2_max <- c()

# Create data frames to store measure of fit in. Keep numbers associated with model type
aic_models <- data.frame(Model = c("Schoolfield full", "Schoolfield low", "Schoolfield high", "Cubic"))
bic_models <- data.frame(Model = c("Schoolfield full", "Schoolfield low", "Schoolfield high", "Cubic"))
r_2_models <- data.frame(Model = c("Schoolfield full", "Schoolfield low", "Schoolfield high", "Cubic"))

for (i in 1:length(Final_ID)) {
  
  # Create a vector of each AIC estimate, if/else statement to filter out missing model fits
  aic_models$Fit <- c(
    if(any(Final_ID[i] == levels(schoolf_param_full$FinalID))) {
      schoolf_param_full[schoolf_param_full$FinalID == Final_ID[i], ]$AIC
    } else {NA},
    if(any(Final_ID[i] == levels(schoolf_param_low$FinalID))) {
      schoolf_param_low[schoolf_param_low$FinalID == Final_ID[i], ]$AIC
    } else {NA},
    if(any(Final_ID[i] == levels(schoolf_param_high$FinalID))) {
      schoolf_param_high[schoolf_param_high$FinalID == Final_ID[i], ]$AIC
    } else {NA},
    if(any(Final_ID[i] == levels(cubic_parameters$FinalID))) {
      cubic_parameters[cubic_parameters$FinalID == Final_ID[i], ]$AIC
    } else {NA}  
  )
  
  # Create a vector of each BIC estimate
  bic_models$Fit <- c(
    if(any(Final_ID[i] == levels(schoolf_param_full$FinalID))) {
      schoolf_param_full[schoolf_param_full$FinalID == Final_ID[i], ]$BIC
    } else {NA},
    if(any(Final_ID[i] == levels(schoolf_param_low$FinalID))) {
      schoolf_param_low[schoolf_param_low$FinalID == Final_ID[i], ]$BIC
    } else {NA},
    if(any(Final_ID[i] == levels(schoolf_param_high$FinalID))) {
      schoolf_param_high[schoolf_param_high$FinalID == Final_ID[i], ]$BIC
    } else {NA},
    if(any(Final_ID[i] == levels(cubic_parameters$FinalID))) {
      cubic_parameters[cubic_parameters$FinalID == Final_ID[i], ]$BIC
    } else {NA}  
  )
  
  # Create a vector of each r-squared value
  r_2_models$Fit <- c(
    if(any(Final_ID[i] == levels(schoolf_param_full$FinalID))) {
      schoolf_param_full[schoolf_param_full$FinalID == Final_ID[i], ]$R_squared
    } else {NA},
    if(any(Final_ID[i] == levels(schoolf_param_low$FinalID))) {
      schoolf_param_low[schoolf_param_low$FinalID == Final_ID[i], ]$R_squared
    } else {NA},
    if(any(Final_ID[i] == levels(schoolf_param_high$FinalID))) {
      schoolf_param_high[schoolf_param_high$FinalID == Final_ID[i], ]$R_squared
    } else {NA},
    if(any(Final_ID[i] == levels(cubic_parameters$FinalID))) {
      cubic_parameters[cubic_parameters$FinalID == Final_ID[i], ]$R_squared
    } else {NA}  
  )
  
  # determine which represents the best fit
  
  aic_max <- c(aic_max, toString(aic_models$Model[which.min(aic_models$Fit)]))
  bic_max <- c(bic_max, toString(bic_models$Model[which.min(bic_models$Fit)]))
  r_2_max <- c(r_2_max, toString(r_2_models$Model[which.min(1 - r_2_models$Fit)]))
  
}

# Summary dataframe: Count number of times each model returns best fit
best_fits <- data.frame(Models = c("Schoolfield full", "Schoolfield low", "Schoolfield high", "Cubic"))
best_fits$rsquared <- table(factor(r_2_max, levels = c("Schoolfield full", "Schoolfield low", "Schoolfield high", "Cubic")))
best_fits$AIC <- table(factor(aic_max, levels = c("Schoolfield full", "Schoolfield low", "Schoolfield high", "Cubic")))
best_fits$BIC <- table(factor(bic_max, levels = c("Schoolfield full", "Schoolfield low", "Schoolfield high", "Cubic")))

## ---- q2 ----
kable(best_fits)

##----q3----

# Percentage of successful fits
prc_full <- find_percent(schoolf_param_full)
prc_low <- find_percent(schoolf_param_low) 
prc_high <- find_percent(schoolf_param_high)
prc_cubic <- find_percent(cubic_parameters)

# For how many trait responses did no model fit?
fit_fail <- (length(Final_ID)-sum(best_fits$rsquared) )

# Mode of Activation Energy in successful fits
mode_full <- find_mode(schoolf_param_full$E)
mode_low <- find_mode(schoolf_param_low$E)
mode_high <- find_mode(schoolf_param_high$E)

##----q4----

# Convert Knitr document from .Rnw to .tex
knit("Write_up.Rnw")