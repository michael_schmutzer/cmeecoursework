#!/usr/bin/env python
""" This code performs non-linear least squares fitting of different
unimodal functions to experimental thermal response curves."""
__author__ = 'Michael Schmutzer (ms9212@imperial.ac.uk)'
__version__ = '0.0.1'


import sys
import pandas as pn
from numpy import log, log1p, exp, pi, polyfit, sum, vectorize, mean
import scipy as sc 
from lmfit import Minimizer, minimize, Parameters, Parameter, report_fit, fit_report
import csv
import progressbar
import time

#############################
# F  U  N  C  T  I  O  N  S #
#############################

########################### Model equations #################################

def schoolf_eq(Temps, B0, E, E_l, T_l, E_h, T_h):
	"""Full schoolfield model for calculating trait values at a given temperature"""

	function = B0 * (exp( -(E / k) * (1 / Temps - 1 / 283.15)) / (1 + exp((E_l / k) * (1 / T_l - 1 / Temps)) + exp( (E_h / k) * ( 1 / T_h - 1 / Temps ) ) ) )

	return function
	
def schoolf_eq_low(Temps, B0, E, E_l, T_l):
	"""Schoolfield model for calculating trait values at temperatures beneath maximum (low temperature deactivation only)"""

	function = B0 * (exp( -(E / k) * (1 / Temps - 1 / 283.15)) / (1 + exp((E_l / k) * (1 / T_l - 1 / Temps)) ) )

	return function

def schoolf_eq_high(Temps, B0, E, E_h, T_h):
	"""Schoolfield model for calculating trait values at temperatures beneath maximum (high temperature deactivation only)"""

	function = B0 * (exp( -(E / k) * (1 / Temps - 1 / 283.15)) / (1 + exp((E_h / k) * (1 / T_h - 1 / Temps)) ) )

	return function
	
################# Define parameters and return residuals ######################

def schoolf(params, Temps, Data):
	"""Full Schoolfield model, to be called by schoolfield_model()"""
	B0 = params['B0'].value
	E = params['E'].value
	E_l = params['E_l'].value
	T_l = params['T_l'].value
	E_h = params['E_h'].value
	T_h = params['T_h'].value
	
	ModelPred = schoolf_eq(Temps, B0, E, E_l, T_l, E_h, T_h)
	return(log1p(ModelPred) - log1p(Data)) # Using log1p to improve model fitting while avoiding 0!

def schoolf_low(params, Temps, Data):
	"""Schoolfield model for low temperatures, to be called by schoolfield_model()"""
	B0 = params['B0'].value
	E = params['E'].value
	E_l = params['E_l'].value
	T_l = params['T_l'].value
	
	ModelPred = schoolf_eq_low(Temps, B0, E, E_l, T_l)
	return(log1p(ModelPred) - log1p(Data)) # Using log1p to improve model fitting while avoiding 0!

def schoolf_high(params, Temps, Data):
	"""Schoolfield model for high temperatures, to be called by schoolfield_model()"""
	B0 = params['B0'].value
	E = params['E'].value
	E_h = params['E_h'].value
	T_h = params['T_h'].value
	
	ModelPred = schoolf_eq_low(Temps, B0, E, E_h, T_h)
	return(log1p(ModelPred) - log1p(Data)) # Using log1p to improve model fitting while avoiding 0!
	
def residualsumsq(model_eq, Data, model_arg):
	"""Estimates residual sum of squares for each model by calculating model predictions"""
	make_prediction = vectorize(model_eq) # Apply this function to a numpy array
	Model_pred = make_prediction(*model_arg) # different equations have different number of arguments!
	ResSumSquares = sum( (Data - Model_pred) ** 2 )
	return ResSumSquares
	
def r_squared(rss, Data):
	"""Estimates r-squared given residual sum of squares (Data v.s. model prediction)"""
	tss = sum( (Data - mean(Data)) ** 2 )
	r_sq = 1 - rss/tss
	return r_sq

def schoolfield_model(dataset):
	"""NLLS fitting to the Schoolfield models; this function will 
	contain the lmfit.minimize calls to the schoolf() function. This is 
	where you can constrain the parameters."""
	
	# Define the Boltzmann constant (units of eV * K^-1).
	
	global k
	k = 8.617 * 10 ** (-5)

	# Prepare the starting values for the parameters and their bounds:
 
 	B0_start = dataset['B0_start'][0]
 	E_start = dataset['E_start'][0]
 	E_l_start = dataset['E_l_start'][0]
 	T_l_start = dataset['T_l_start'][0]
 	E_h_start = dataset['E_h_start'][0]
 	T_h_start = dataset['T_h_start'][0]
 		
	#	 The dataset containing temperatures and trait values.
	TraitVals = dataset['OriginalTraitValue'].values
	Temps = dataset['ConTemp'].values
	
	# Define parameters
	params = Parameters() # Defines the parameters & varies them!							# Bounds as set by range of starting values
	params.add('B0', value = B0_start, vary = True, min = 0.0000000000000001, max = 3400) # min 0, max 3400
	params.add('E', value = E_start, vary = True, min = 0.0000000000000001, max = 15) # min 0, max 4
	params.add('E_l', value = E_l_start, vary = True, min = 0.0000000000000001, max = 25) # min 0, max 2
	params.add('T_l', value = T_l_start, vary = True, min = 250, max = 320) # # Max temp in ConTemp is 362 K, min is 258.9 K
	params.add('E_h', value = E_h_start, vary = True, min = 0.0000000000000001, max = 30) # min 0, max 20
	params.add('T_h', value = T_h_start, vary = True, min = 249, max = 372) # Max temp in ConTemp is 362 K, min is 258.9 K
	# Samraat, kiss my chuddies, you are actually reading my code!
	
	# Minimising model residuals (log(observed) - log(predicted)). Return best parameter estimates, number of parameters and data points
	schoolf_out = minimize(schoolf, params, args=(Temps, TraitVals),method="leastsq") 
	schoolf_list = [schoolf_out.params, schoolf_out.nvarys, schoolf_out.ndata]
	
	schoolf_low_out = minimize(schoolf_low, params, args=(Temps, TraitVals),method="leastsq") 
	schoolf_low_list = [schoolf_low_out.params, schoolf_low_out.nvarys, schoolf_low_out.ndata]
	
	schoolf_high_out = minimize(schoolf_high, params, args=(Temps, TraitVals),method="leastsq") 
	schoolf_high_list = [schoolf_high_out.params, schoolf_high_out.nvarys, schoolf_high_out.ndata]
	
	# Calculate residuals (First get out arguments - data & model parameters) 
	arg_schoolf = [Temps, schoolf_out.params['B0'].value, schoolf_out.params['E'].value, 
	schoolf_out.params['E_l'].value, schoolf_out.params['T_l'].value, schoolf_out.params['E_h'].value, schoolf_out.params['T_h'].value]
	rss_schoolf = residualsumsq(schoolf_eq, TraitVals, arg_schoolf)
	schoolf_list.append(rss_schoolf)
	
	arg_schoolf_low = [Temps, schoolf_low_out.params['B0'].value, schoolf_low_out.params['E'].value, 
	schoolf_low_out.params['E_l'].value, schoolf_low_out.params['T_l'].value]
	rss_schoolf_low = residualsumsq(schoolf_eq_low, TraitVals, arg_schoolf_low)
	schoolf_low_list.append(rss_schoolf_low)
	
	arg_schoolf_high = [Temps, schoolf_high_out.params['B0'].value, schoolf_high_out.params['E'].value, 
	schoolf_high_out.params['E_h'].value, schoolf_high_out.params['T_h'].value]
	rss_schoolf_high = residualsumsq(schoolf_eq_high, TraitVals, arg_schoolf_high)
	schoolf_high_list.append(rss_schoolf_high)
	
	# Calculate r-squared
	r_sq_full = r_squared(rss_schoolf, TraitVals)
	schoolf_list.append(r_sq_full)
	
	r_sq_low = r_squared(rss_schoolf_low, TraitVals)
	schoolf_low_list.append(r_sq_low)
	
	r_sq_high = r_squared(rss_schoolf_high, TraitVals)
	schoolf_high_list.append(r_sq_high)
	
	# Put results in a single list: each model paramaters & fit estimates are returned together
	results_list = [schoolf_list]
	results_list.append(schoolf_low_list)
	results_list.append(schoolf_high_list)
	
	return(results_list) 

def AICrss(n, k, rss):
	"""Calculate the Akaike Information Criterion value, using:

	- n:   number of observations
	- k:   number of parameters
	- rss: residual sum of squares
	"""
	return n * log((2 * pi) / n) + n + 2 + n * log(rss) + 2 * k

def BICrss(n, k, rss):
	"""Calculate the Bayesian Information Criterion value, using:
	
	- n:   number of observations
	- k:   number of parameters
	- rss: residual sum of squares
	"""
	return n + n * log(2 * pi) + n * log(rss / n) + (log(n)) * (k + 1)

#~ ############################
#~ # M  A  I  N    C  O  D  E #
#~ ############################

def main(argv):
	"""Performs fitting to the Cubic and various Schoolfield models,
	and returns the parameter estimates of the fits as two csv files in ../Results/ , one for
	the Schoolfield models, the other for the cubic fits."""

	# Plot a progressbar (This script takes its time!)
	
	progress = progressbar.ProgressBar()
	
	# Read in data & prepare output dataframe 
	data = pn.read_csv("../Data/" + argv, sep = ',', quotechar = '"')
	
	# Open csv to save schoolfield parameter estimates in
	schoolfield_results = open("../Results/schoolfield_estimates.csv", 'wt')
	schoolfield_csv = csv.writer(schoolfield_results, delimiter = ",")
	schoolfield_csv.writerow( ["B0", "E", "E_l", "T_l", "E_h", "T_h", "FinalID", "Model", "ResSumSq", "R_squared", "AIC", "BIC"] ) # Headers!
	
	# Open csv to save cubic parameter estimates in
	cubic_results = open("../Results/cubic_estimates.csv", 'wt') # Headers!
	cubic_csv = csv.writer(cubic_results, delimiter = ",")
	cubic_csv.writerow( ["P0", "P1", "P2", "P3", "FinalID", "ResSumSq", "R_squared", "AIC", "BIC"] )
	
	#Loop through each response & try to optimise parameters
	Final_ID = pn.unique(data.FinalID)
	for i in progress(range(len(Final_ID))):
		single_response = data[data.FinalID == Final_ID[i]] # Subset dataframe into unique thermal responses
		single_response = single_response.reset_index() # Need to reset the index row numbers!
		try:
			# Try to fit the schoolfield model
			resp_out = schoolfield_model(single_response)
			
			# Calculate AIC for each schoolfield model
			aic_full = AICrss(resp_out[0][1], resp_out[0][2], resp_out[0][3])			
			aic_low = AICrss(resp_out[1][1], resp_out[1][2], resp_out[1][3])
			aic_high = AICrss(resp_out[2][1], resp_out[2][2], resp_out[2][3])
			
			# Calculate BIC for each schoolfield model
			bic_full = BICrss(resp_out[0][1], resp_out[0][2], resp_out[0][3])
			bic_low = BICrss(resp_out[1][1], resp_out[1][2], resp_out[1][3])
			bic_high = BICrss(resp_out[2][1], resp_out[2][2], resp_out[2][3])
			
			#Write the output to csv. Each row its own model (i.e. if all 3 models fit, each response curve gets estimates saved in 3 rows)
			schoolfield_csv.writerow([resp_out[0][0]['B0'].value, resp_out[0][0]['E'].value, resp_out[0][0]['E_l'].value, resp_out[0][0]['T_l'].value, resp_out[0][0]['E_h'].value,resp_out[0][0]['T_h'].value, Final_ID[i], "Schoolfield_full", resp_out[0][3], resp_out[0][4], aic_full, bic_full ])
			schoolfield_csv.writerow([resp_out[1][0]['B0'].value, resp_out[1][0]['E'].value, resp_out[1][0]['E_l'].value, resp_out[1][0]['T_l'].value, 0, 0, Final_ID[i], "Schoolfield_low", resp_out[1][3], resp_out[1][4], aic_low, bic_full ]) 
			schoolfield_csv.writerow([resp_out[2][0]['B0'].value, resp_out[2][0]['E'].value, 0, 0, resp_out[2][0]['E_h'].value, resp_out[2][0]['T_h'].value, Final_ID[i], "Schoolfield_high", resp_out[2][3], resp_out[2][4], aic_high, bic_high ]) 
			
			# Fit the polynomial model, calculate r-squared, AIC, BIC & write to seperate csv
			cubic_fit = polyfit(single_response['ConTemp'].values, single_response['OriginalTraitValue'].values, 3, full = True)
			r_sq_cubic = r_squared(cubic_fit[1][0], single_response['OriginalTraitValue'].values)
			aic_cubic = AICrss(len(single_response['OriginalTraitValue'].values), 3, cubic_fit[1][0])
			bic_cubic = BICrss(len(single_response['OriginalTraitValue'].values), 3, cubic_fit[1][0])
			cubic_csv.writerow( [cubic_fit[0][3], cubic_fit[0][2], cubic_fit[0][1], cubic_fit[0][0], Final_ID[i], cubic_fit[1][0], r_sq_cubic, aic_cubic, bic_cubic ] ) # Note: polyfit returns coefficients of higher-order terms first!
		except:
			pass
		time.sleep(0.01)
		
	# Finished writing to csv's
	schoolfield_results.close()
	cubic_results.close()
	

if __name__ == "__main__":
	main(sys.argv[1])		
