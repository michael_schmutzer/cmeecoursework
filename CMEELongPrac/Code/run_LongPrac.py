#!/usr/bin/env python
"""Python script to run whole project, from data curation to compilation
of LaTeX report. Creates results directory. Script runs:
				Data_curation.R
				ThermalResponse.py
				Model_plotting.R
				Write_up.Rnw
				Write_up.tex
"""
__author__ = "Michael Schmutzer (ms9212@ic.ac.uk)"
__version_ = "0.0.1"

import subprocess

# Define series of commands to run in order to compile LaTeX file

CompileLaTeX = []
# Word count. Compile knitr, then .tex file
CompileLaTeX.append("texcount -inc -incbib -sum -0 -template={SUM} Write_up.tex | grep \"[0-9]\" > count.txt")
CompileLaTeX.append("Rscript -e \"library(knitr); knit('Write_up.Rnw')\"")
CompileLaTeX.append("pdflatex Write_up.tex")
CompileLaTeX.append("pdflatex Write_up.tex")
CompileLaTeX.append("bibtex Write_up")
CompileLaTeX.append("pdflatex Write_up.tex")
CompileLaTeX.append("pdflatex Write_up.tex")
# Remove temporary files
CompileLaTeX.append("rm *.aux")
CompileLaTeX.append("rm *.log")
CompileLaTeX.append("rm *.blg")
CompileLaTeX.append("rm *.bbl")
CompileLaTeX.append("rm count.txt")
CompileLaTeX.append("rm *concordance.tex")
CompileLaTeX.append("mv Write_up.pdf ../Results/")
CompileLaTeX.append("evince ../Results/Write_up.pdf &")

# Make results directories
subprocess.os.system("mkdir ../Results/")

subprocess.os.system("mkdir ../Results/Model_plots/")

subprocess.os.system("Rscript Data_curation.R")

subprocess.os.system("python ThermalResponse.py ThermResp_startvals.csv")

subprocess.os.system("Rscript Model_plotting.R")


for i in range(len(CompileLaTeX)):
	subprocess.os.system(CompileLaTeX[i])
