Week4 contains worked examples of statistical tests in R, closely following
instructions saved in ./Practicals. Sample data is in ./Data, ./Output 
contains graphics produced in R.

R scripts in ./Code/			Topics
---------------------------
Practical1.R					# Data types & manipulation
Practical2.R					# Data frames, input / output 
Practical3.R					# Base plots
Practical4.R					# t-test & chi-squared test
Practical5.R					# t & Wilcoxon tests, correlation 
Practical6.R					# ANOVA & linear regression
Practical7.R					# Multivariate models
Practical8.R					# Model criticism simplification
