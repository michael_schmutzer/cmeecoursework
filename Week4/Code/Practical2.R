# Practical 2 - Importing data

# Making some sequences

1:10
-4:4
12:3

# In steps of 2...
seq(from = 1, to = 20, by = 2)
seq(from = 1, to = 3, length = 10)

# Repeat values in a vector
rep(x = 1:4, times = 2)
rep(x = 1:4, each = 2)
rep(x = 1:4, times = 1:4)

# Factors
myCharacterVector <- c("low","low", "low", "low"
           , "high", "high", "high","high")
myFactor <- as.factor(myCharacterVector)
str(myFactor)

# Generating levels in a factor using gl()
myFactor <- gl(n = 2, k = 4)
print(myFactor)

myFactor <- gl(n = 2, k = 2, length = 8, labels = c("low", "high"))
print(myFactor)

############# Making a Data frame from vectors ####################

# Data vectors
MyNumerics <- c(1.3, 2.5, 1.9, 3.4, 5.6, 1.4, 3.1, 2.9)
MyCharacters <- gl(n = 2, k = 2, length = 8, labels = c("low", "high"))
MyLogicals <- c(T,T,F,F,T,T,F,F)

# Assemble a dataframe
MyData <- data.frame(num = MyNumerics, char = MyCharacters, logical = MyLogicals)
print(MyData)
str(MyData)

# Keep characters as strings, not factors
MyData <- data.frame(num = MyNumerics, char = MyCharacters, logical = MyLogicals, stringsAsFactors = FALSE)
str(MyData)
print(MyData)

# Reading input from .csv file

factories <- read.csv("../Data/factories.csv")
str(factories)

factories$rate
# using logical test

factories$factory == "Sheffield"
factories$rate[factories$factory == "Sheffield"]
factories$treatment == "C"
factories$rate[factories$treatment == "C"]

# Creating a new column

factories$country <- "England"
factories$country[factories$factory == "Gwent"] <- "Wales"
factories$country[factories$factory == "Newport"] <- "Wales"
str(factories)

# attach() commands ... access data in data frame without
# needing to call it's name
attach(factories)
rate[treatment == "C"]
rate[factory == "Sheffield"]
detach(factories)

# for single lines (i.e. in tapply)
with(factories, rate[factory == "Sheffield"])


# Using subset

WelshData <- subset(factories, subset = country == "Wales")
str(WelshData)

WelshData <- subset(factories, subset = country == "Wales", select = rate)
str(WelshData)
