# Multivariate models

daphnia <- read.delim("../Data/daphnia.txt")
summary(daphnia)

# Diagnostic plots
par(mfrow = c(1, 2))
plot(Growth.rate ~ Detergent, data = daphnia)
plot(Growth.rate ~ Daphnia, data = daphnia)

# Define function to estimate standard error
seFun <- function(x){
  sqrt(var(x)/length(x))
}

# Plot a barplot with error bars (standard error)
detergentMean <- with(daphnia, tapply(Growth.rate, INDEX = Detergent,
                                      FUN = mean))
detergentSEM <- with(daphnia, tapply(Growth.rate, INDEX = Detergent,
                                     FUN = seFun))
cloneMean <- with(daphnia, tapply(Growth.rate, INDEX = Daphnia, 
                                  FUN = mean))
cloneSEM <- with(daphnia, tapply(Growth.rate, INDEX = Daphnia, FUN = seFun))

par(mfrow = c(2,1), mar = c(4, 4, 1, 1))
barMids <- barplot(detergentMean,
                  xlab = "Detergent type",
                  ylab = "Population growth rate",
                  ylim = c(0, 5))
arrows(barMids, detergentMean - detergentSEM, barMids, 
      detergentMean + detergentSEM, code = 3, angle = 90)

barMids <- barplot(cloneMean, xlab = "Daphnia clone",
                  ylab = "Population growth rate",
                  ylim = c(0, 5))
arrows(barMids, cloneMean - cloneSEM, barMids, cloneMean + cloneSEM,
       code =3, angle = 90)

# Making a model to check for additive effects

daphniaMod <- aov(Growth.rate ~ Detergent + Daphnia, data = daphnia)
summary(daphniaMod)
summary.lm(daphniaMod)

detergentMean - detergentMean[1]

# Tukey's HSD ... are factors sign different from one another?
daphniaModHSD <- TukeyHSD(daphniaMod)
daphniaModHSD

par(mfrow = c(1,2), mar = c(5, 8, 2, 2), las = 1)
plot(daphniaModHSD)

############## Multiple regression ###################

timber <- read.delim("../Data/timber.txt")
summary(timber)

# Exploratory plots

pairs(timber)
cor(timber)

# Model fitting, is simplification justified?

timberMod <- lm(volume ~ girth + height, data = timber)
summary.aov(timberMod)
summary(timberMod)

################# Analysis of Covariance ##################

plantGrowth <- read.delim("../Data/ipomopsis.txt")
summary(plantGrowth)

# Model fitting

plantMod <- lm(Fruit ~ Root + Grazing, data = plantGrowth)
anova(plantMod)
summary(plantMod)

# Plot model

par(mfrow = c(1,1))

plantGrowth$plotChar <- 1
plantGrowth$plotChar[plantGrowth$Grazing == "Ungrazed"] <- 19
plantGrowth$plotCol <- "red"
plantGrowth$plotCol[plantGrowth$Grazing == "Ungrazed"] <- "darkblue"

plot(Fruit ~ Root, data = plantGrowth, 
     ylab = "Fruit production (dry weight, mg)",
     xlab = "Initial root diameter (mm)",
     pch = plotChar,
     col = plotCol)
# Add regression line
abline(a = -127.829, b = 23.56, col = "red")
abline(a = -127.829 + 36.103, b = 23.56, col = "blue")
