#!/usr/bin/Rscript

# Author = Michael Schmutzer (michael.schmutzer12@ic.ac.uk)

# Run neutral simulations in parallel

# Clear workspace
rm(list = ls())
graphics.off()

iter <- as.numeric(Sys.getenv("PBS_ARRAY_INDEX"))


################# Neutral theory simulation #####################

species_richness <- function(x) {
  # Returns number of species in community, ignores abundance 
  length(unique(x))
}

initialise_max <- function(n) {
  # Generates maximum community of size n with n species
  community <- seq(from = 1, to = n, by = 1)
  return(community)
}

initialise_min <- function(n) {
  # Generates minimum community of size n
  community <- seq(from = 1, to = 1, length.out = n)
  return(community)
}

choose_two <- function(x) {
  # Pick individual to be replaced & individual providing offspring
  sample(x = length(x), size = 2, replace = FALSE)
}

neutral_step <- function(Community) {
  # Neutral replacement of one indvidual by offspring of another
  mors <- choose_two(Community)
  Community[mors[1]] <- Community[mors[2]]
  return(Community)
}

neutral_time_series <- function(Initial, Duration, Interval) {
  # Returns list containing (a) Species richness for each sampling
  # interval and (b) final community composition
  community <- Initial
  richness_time_series <- species_richness(community)
  for (step in 1:Duration) {
    community <- neutral_step(community)
    if (step %% Interval == 0) {
      richness_time_series <- append(richness_time_series,
                                     species_richness(community))
    }
  }
  time_list <- list()
  time_list[[1]] <- richness_time_series
  time_list[[2]] <- community
  return(time_list)
}

question_7 <- function() {
  # Plots richness time series with default parameters
  init <- initialise_max(100)
  tempus_fugit <- neutral_time_series(init, 10000, 10)
  x <- seq(from = 0, to = 10000, by = 10)
  y <- tempus_fugit[[1]]
  plot(x, y, xlab = "Time", ylab = "Species richness", type = "l")
}

neutral_step_speciation <- function(Community, v) {
  # Replaces individuals in community with a member of an existing
  # or a new species. The latter occurs at probability v.
  fortuna <- runif(1, min = 0, max = 1)
  if (fortuna > v) {
    # Replace individual with existing species
    mors <- choose_two(Community)
    Community[mors[1]] <- Community[mors[2]]
  } 
  else {
    # Replace individual with new species
    mors <- sample(x = length(Community), size = 1)
    nativitas <- max(Community) + 1
    Community[mors] <- nativitas
  }
  return(Community)
}

neutral_time_series_speciation <- function(Initial, Duration, Interval, v) {
  # Returns list containing (a) Species richness for each sampling
  # interval and (b) final community composition with speciation
  # rate v
  community <- Initial
  richness_time_series <- species_richness(community)
  for (step in 1:Duration) {
    community <- neutral_step_speciation(community, v)
    if (step %% Interval == 0) {
      richness_time_series <- append(richness_time_series,
                                     species_richness(community))
    }
  }
  time_list <- list()
  time_list[[1]] <- richness_time_series
  time_list[[2]] <- community
  return(time_list)
}

question_10 <- function() {
  # Plots richness time series of minimum and maximum communities
  # under neutral model with speciation. Uses defaults. 
  Init_max <- initialise_max(100)
  Init_min <- initialise_min(100)
  tempus_fugit_max <- neutral_time_series_speciation(Init_max, 10000, 10, 0.1)
  tempus_fugit_min <- neutral_time_series_speciation(Init_min, 10000, 10, 0.1)
  x <- seq(from = 0, to = 10000, by = 10)
  plot(x, tempus_fugit_max[[1]], xlab = "Time", ylab = "Species richness", type = "l")
  lines(x, tempus_fugit_min[[1]], col = "red")
}

species_abundance <- function(Community) {
  # Returns vector of species abundances in decending order
  Abundance <- sort(as.vector(table(Community)), decreasing = TRUE)
  return(Abundance)
}

octaves <- function(Abundance) {
  # Returns the number of species whose abundance lies between 
  # 2^n-1 and 2^n
  bin_number <- ceiling(log2(max(Abundance + 1)))
  breaks <- 2^seq(from = 0, to = bin_number, by = 1)
  Octave <- c()
  for (i in 2:(bin_number + 1)) {
    Octave[i-1] <- sum(breaks[i-1] <= Abundance[Abundance < breaks[i]])
  }
  return(Octave)
}

sum_vect <- function(x, y) {
  # Adds two vectors. If one is shorter, appends 0 to match the other.
  if (length(x) > length(y)) {
    lack <- length(x) - length(y)
    complement <- seq(from = 0, to = 0, length.out = lack)
    y <- append(y, complement)
  } 
  else if (length(y) > length(x)) {
    lack <- length(y) - length(x)
    complement <- seq(from = 0, to = 0, length.out = lack)
    x <- append(x, complement)
  }
  z <- x + y
  return(z)
}

question_14 <- function() {
  # Plots average of 100 octave samples, generated after an initial 
  # burn-in of 10,000 iterations
  init <- initialise_max(100)
  tempus_fugit <- neutral_time_series_speciation(init, 10000, 10, 0.1)
  octave_sum <- c()
  # Sum octaves for 100 runs of 1000 iterations
  for (run in 1:100) {
    tempus_fugit <- neutral_time_series_speciation(tempus_fugit[[2]], 1000, 10, 0.1)
    octave_sum <- sum_vect(octave_sum, octaves(species_abundance(tempus_fugit[[2]])))
  }
  octave_average <- octave_sum / 100
  barplot(octave_average, xlab = "Abundance octaves (Max. number of individuals
          per species)", 
          ylab = "Average number of species")
}

#################### Simulation using HPC ######################

neutral_speciation_cluster <- function(Initial, Duration, v) {
  # Runs neutral model with speciation, returns community, species
  # richness, and abundace octaves at the last iteration
  community <- Initial

  for (step in 1:Duration) 
  {
    community <- neutral_step_speciation(community, v)
  }
  time_list <- list()
  time_list[[1]] <- community
  time_list[[2]] <- species_richness(community)
  time_list[[3]] <- octaves(species_abundance(community))
  return(time_list)
}

neutral_simulation <- function(speciation_rate, size, wall_time, burn_in_time, interval_rich, interval_oct) {
  # Runs neutral simulation for a total running time given in minutes. 
  # Allows simulation to burn in for a specified number of generations. 
  # Samples richness while burning in, and abundance throughout the
  # simulation, each at their own interval. The number of model iterations
  # per generation is equal to the population size. Returns a list of:
  #       A) Richness time series during burn-in time
  #       B) Abundance octaves at equilibrium
  #       C) Final community composition
  #       D) Elapsed running time (in seconds)
  community <- initialise_max(size)
  tempus_fugit <- list(community)
  richness_time_series <- species_richness(community)
  octaves_time_series <- list(octaves(species_abundance(community)))
  generation <- 1
  start_time <- proc.time()[3] 
  run_time <- 0
  while (run_time < (60 * wall_time)) 
  {
    tempus_fugit <- neutral_speciation_cluster(tempus_fugit[[1]],
                                               size,
                                               speciation_rate)
    if (generation <= burn_in_time)
    {
      if (generation %% interval_rich == 0)
      {
        richness_time_series <- append(richness_time_series, tempus_fugit[[2]])
      }
    }
    if (generation %% interval_oct == 0) 
    {
      octaves_time_series <- c(octaves_time_series, list(tempus_fugit[[3]]))
    }
    generation <- generation + 1
    run_time <- proc.time()[3] - start_time
  }  
  run_results <- list()
  run_results[[1]] <- richness_time_series
  run_results[[2]] <- octaves_time_series
  run_results[[3]] <- tempus_fugit[[1]]
  run_results[[4]] <- run_time
  return(run_results)
}

cluster_run <- function(speciation_rate, size, wall_time, burn_in_time,
                        rand_seed, interval_rich, interval_oct) {
  # Run neutral simulation with specified seed. Saves simulation
  # results and run parameters. 
  set.seed(rand_seed)
  Run_results <- neutral_simulation(speciation_rate, size, wall_time, burn_in_time, interval_rich, interval_oct)
  Run_results[[5]] <- c(speciation_rate, size, wall_time, rand_seed, interval_rich, interval_oct, burn_in_time)
  save(Run_results, file = paste("Neutral_simulation_", rand_seed, ".RData", sep = ""))
}

################### Run simulation #########################

determine_size <- function(Iter) {
# Determine population size given identity of parallel run
  sizes <- c(500, 1000, 2500, 5000)
  Ident <- rep(1:4, 25)[Iter]
  size <- sizes[Ident]
  return(size)
}

Size <- determine_size(iter)

cluster_run(0.004362, Size, 60 * 11.5, 4 * Size, iter, 1, Size / 10) # To run in cluster

# cluster_run(0.004362, Size, 0.5, 4 * Size, iter, 1, Size / 10) # Practice run