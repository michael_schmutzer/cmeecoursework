Week6
-----------------

Scripts in ./Code/
-----------------------------
debugme.py			# Example of a bug in python
fmr.R				# Fits & plots linear model to ./Data/NagyEtAl1999.csv, saves plot
lmfitex1.py			# Fit sine linear model to climate data in ./Data/ClimateData.csv
LV1.py				# Continuous time Lotka-Volterra model of predation
LV2.py				# Continuous time Lotka-Volterra model with prey density dependence and optional command line input
LV3.py				# Discrete time Lotka-Volterra model with prey density dependence, command line input
LV4.py				# As LV3.py, but with gaussian fluctuations in prey growth rate
LV5.py				# As LV4.py, with gaussian fluctuations in both predator and prey growth rates
profileme.py		# Sample loops for demonstrating profiling
pythonsql.py		# Creating and accessing SQLITE database from python
run_fmr_R.py		# Running fmr.R from python using subprocess, returns R console output and whether the run was successful
run_LV.py			# Runs and profiles LV models
SQLinR.R			# Creating and accessing SQLITE database from R
test_oaks.py		# Search for oak genus in ./Data/TestOaksData.csv, return only oak genus & species name in new .csv file
TestR.py			# Print message to R console
TestR.R				# Run TestR.R, return R output or error messages in new files
using_os.py			# Search for files and directories recursively from home directory, returns only those files and/or directories starting with a capital or lower case C. 
