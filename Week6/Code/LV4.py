#!/usr/bin/env python

"""A discrete Lotka-Volterra Model with gaussian fluctuations in resource growth rate, simulated using scipy."""

__author__ = "Michael Schmutzer (michael.schmutzer12@ic.ac.uk)"
__version__ = "0.0.1"

import sys
import scipy as sc 
import scipy.integrate as integrate
import numpy.random as nr

import pylab as p #Contains matplotlib for plotting

# import matplotlip.pylab as p #Some people might need to do this

def define_parameters(x):
	"""Takes arguments from command line, returns them in a list. If no 
	arguments are given, default values are used. Replaces defaults with
	arguments in the order r, a, z, e."""
	
	lv_params = [1.5, 0.1, 0.5, 0.75]
	if len(x) == 1:
		return lv_params
	elif len(x) >= 6:
		print("Error: Too many arguments given. Please enter no more than four.")
		sys.exit()
	else:
		for i in range((len(x)-1)):
			lv_params[i] = float(x[i + 1])
		return lv_params
	return lv_params

LV_params = define_parameters(sys.argv)

def LV_discrete(pops, t):
    """ Returns predator and prey populations at any 
    given time step based on the previous step"""
    
    for yr in range(t-1):
		E = nr.normal(loc = 0.0, scale = 0.2)
		pops[yr + 1, 0] = pops[yr, 0] * (1 + (r + E) * (1 - (pops[yr, 0]/K)) - a * pops[yr, 1])
		pops[yr + 1, 1] = pops[yr, 1] * (1 - z + e * a * pops[yr, 0])
    return pops

# Define parameters, take from command line or default:
r = LV_params[0] # Resource growth rate
a = LV_params[1] # Consumer search rate (determines consumption rate) 
z = LV_params[2] # Consumer mortality rate
e = LV_params[3] # Consumer production efficiency
K = 20.


# Now define time:
t = 100

# Initialize array:
z0 = sc.zeros((t, 2)) # Create an empty array
z0[0][0] = 10.
z0[0][1] = 5.

pops = LV_discrete(z0, t)

prey, predators = pops.T # . T transposes array, 1 , 2 refers to rows
f1 = p.figure() #Open empty figure object
p.plot(range(t), prey, 'g-', label='Resource density') # Plot
p.plot(range(t), predators  , 'b-', label='Consumer density ')
p.grid()
p.legend(loc='best')
p.xlabel('Time')
p.ylabel('Population')
p.title('Consumer-prey population dynamics')
p.text(x = 80, y = 2.5, s = "r = %r\na = %r\nz = %r\ne = %r" % (r, a, z, e))
#p.show() # Activate to see plot
f1.savefig('../Results/prey_and_predators_LV4.pdf') #Save figure
print("Population densities at end of model run")
print("Predator:" )
print(predators[-1])
print("Prey:")
print(prey[-1])

sys.exit()

