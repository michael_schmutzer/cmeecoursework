#!/user/bin/env python

"""Run R script fmr.R from pyton. Returns R Console output and if run is successful."""

__author__ = "Michael Schmutzer (michael.schmutzer12@ic.ac.uk)"
__version_ = "0.0.1"

import subprocess

# Run r script. Using stdout, etc. allows subprocess.Popen() to access R output, and return it via .wait() or .communicate()

Rrun = subprocess.Popen("/usr/lib/R/bin/Rscript fmr.R", stdout = subprocess.PIPE, stdin = subprocess.PIPE, stderr = subprocess.PIPE, shell = True).communicate()

# Check if subprocess ran as expected. .communicate() returns a tuple with, in its second position, error messages from R
# If everything is all right, print R console output (string in first position in tuple)
if Rrun[1] == "":
	print(Rrun[0])
	print("Run succesful! Don the beer goggles!")
else:
	print("Something's wrong! Where is the fly swatter?")

