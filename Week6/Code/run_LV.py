#!/usr/bin/env python

"""Runs two versions of the Lotka-Volterra predation model and compares
running time."""

import subprocess

subprocess.os.system("python -m cProfile LV1.py")
subprocess.os.system("python -m cProfile LV2.py 2.5 0.004 1.0 0.66")
subprocess.os.system("python -m cProfile LV3.py 1.2 0.1 0.4 0.8")
subprocess.os.system("python -m cProfile LV4.py 1.4 0.08 0.3 0.95")
subprocess.os.system("python -m cProfile LV5.py 1.4 0.11 0.45 0.6")

