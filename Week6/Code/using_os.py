#!/usr/bin/env python

""" Returns all file and/or directory names starting with capital or 
lower case c."""

__author_ = "Michael Schmutzer (michael.schmutzer12@ic.ac.uk)"
__version__ = "0.0.1"

import subprocess
import re

#################################
#~Get a list of files and 
#~directories in your home/ that start with an uppercase 'C'

# Get the user's home directory.
home = subprocess.os.path.expanduser("~") 

# Create a list to store the results.
FilesDirsStartingWithC = []

# Use a for loop to walk through the home directory.
names_in_directory = [] # Set up an intermediate list
for (dir, subdir, files) in subprocess.os.walk(home):
	for name in subprocess.os.listdir(dir): # Another loop to get dir (directories) contents
		names_in_directory.append(name) # Fill intermediate list with directory contents
		
# Search list of directory contents for names starting with capital C
FilesDirsStartingWithC = re.findall(r'\'C\w*\'', str(names_in_directory)) 
print("Files/Directories staring with C")
print(FilesDirsStartingWithC)

#################################
# Get files and directories in your home/ that start with either an 
# upper or lower case 'C'

# Can use same intermediate files/directories list as last time!
FilesDirsStartingWithCc = re.findall(r'\'[Cc]\w*\'', str(names_in_directory))
print("")
print("")
print("Files/Directories starting with C/c")
print(FilesDirsStartingWithCc)

#################################
# Get only directories in your home/ that start with either an upper or 
#~lower case 'C' 

dir_names = [] # Another intermediate list

# Access directory names straight from .os.walk
for (dir, subdir, files) in subprocess.os.walk(home):
	for item in subdir:
		dir_names.append(item)
	
DirsStartingWithCc = re.findall(r'\'[Cc]\w*\'', str(dir_names))
print("")
print("")
print("Directories starting with C/c")
print(DirsStartingWithCc)
