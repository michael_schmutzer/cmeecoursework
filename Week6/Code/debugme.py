#!/user/bin/env python

"""Example of a bug in python"""

__author__ = 'Michael Schmutzer (michael.schmutzer12@ic.ac.uk)'
__version__ = '0.0.1'

def createabug(x):
	y = x**4
	z = 0.
	y = y/z # Dividing by zero
	return y
	
createabug(25)
