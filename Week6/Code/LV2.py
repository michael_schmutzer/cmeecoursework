#!/usr/bin/env python

"""Lotka-Volterra Model with consumer density-dependence simulated using scipy"""

__author__ = "Michael Schmutzer (michael.schmutzer12@ic.ac.uk)"
__version__ = "0.0.1"

import sys
import scipy as sc 
import scipy.integrate as integrate

import pylab as p #Contains matplotlib for plotting

# import matplotlip.pylab as p #Some people might need to do this


def define_parameters(x):
	"""Takes arguments from command line, returns them in a list. If no 
	arguments are given, default values are used. Replaces defaults with
	arguments in the order r, a, z, e."""
	
	lv_params = [1., 0.001, 1.5, 0.75]
	if len(x) == 1:
		return lv_params
	elif len(x) >= 6:
		print("Error: Too many arguments given. Please enter no more than four.")
		sys.exit()
	else:
		for i in range((len(x)-1)):
			lv_params[i] = float(x[i + 1])
		return lv_params
	return lv_params

LV_params = define_parameters(sys.argv)

def dR_dt(pops, t=0):
    """ Returns the growth rate of predator and prey populations at any 
    given time step """
    
    R = pops[0]
    C = pops[1]
    dRdt = r*R*(1-R/K) - a*R*C 
    dydt = -z*C + e*a*R*C
    
    return sc.array([dRdt, dydt])

# Define parameters, take from command line or default:
r = LV_params[0] # Resource growth rate
a = LV_params[1] # Consumer search rate (determines consumption rate) 
z = LV_params[2] # Consumer mortality rate
e = LV_params[3] # Consumer production efficiency
K = 7000.

# Now define time -- integrate from 0 to 15, using 1000 points:
t = sc.linspace(0, 20,  1000)

x0 = 1000
y0 = 50
z0 = sc.array([x0, y0]) # initials conditions: 10 prey and 5 predators per unit area

pops, infodict = integrate.odeint(dR_dt, z0, t, full_output=True)

infodict['message']     # >>> 'Integration successful.'

prey, predators = pops.T # . T transposes array, 1 , 2 refers to rows
f1 = p.figure() #Open empty figure object
p.plot(t, prey, 'g-', label='Resource density') # Plot
p.plot(t, predators  , 'b-', label='Consumer density ')
p.grid()
p.legend(loc='best')
p.xlabel('Time')
p.ylabel('Population')
p.title('Consumer-prey population dynamics')
p.text(x = 15.5, y = 1090, s = "r = %r\na = %r\nz = %r\ne = %r" % (r, a, z, e))
# p.show() # Activate to see plot
f1.savefig('../Results/prey_and_predators_LV2.pdf') #Save figure
print("Population densities at end of model run")
print("Predator:" )
print(predators[-1])
print("Prey:")
print(prey[-1])

sys.exit()

